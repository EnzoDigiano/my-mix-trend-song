//
//  PlayerStackView.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 03/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import UIKit

class PlayerStackView : UIStackView {
    
    @IBInspectable var backgrounColor: UIColor = UIColor.clear {
        didSet {
            _bkgColor = backgrounColor
        }
    }
    
    private var _bkgColor: UIColor?
    
    override public var backgroundColor: UIColor? {
        get { return _bkgColor }
        set {
            _bkgColor = newValue
            setNeedsLayout()
        }
    }
    
    private lazy var backgroundLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        self.layer.insertSublayer(layer, at: 0)
        return layer
    }()
    
    private lazy var backgroundLayer2: CAShapeLayer = {
        let layer = CAShapeLayer()
        self.layer.insertSublayer(layer, at: 0)
        return layer
    }()
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        backgroundLayer.path = UIBezierPath(rect: self.bounds).cgPath
        backgroundLayer.fillColor = UIColor.clear.cgColor

        backgroundLayer.shadowColor = self.backgroundColor?.cgColor
        backgroundLayer.shadowOffset = CGSize(width: 0, height: 1.0)
        backgroundLayer.shadowRadius = -1
        backgroundLayer.shadowOpacity = 1
        backgroundLayer.masksToBounds = false
        backgroundLayer.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 50).cgPath
        backgroundLayer.cornerRadius = 50
        
        backgroundLayer2.path = UIBezierPath(rect: self.bounds).cgPath
        backgroundLayer2.fillColor = UIColor.clear.cgColor

        backgroundLayer2.shadowColor = UIColor.red.cgColor
        backgroundLayer2.shadowOffset = CGSize(width: 0, height: 1.0)
        backgroundLayer2.shadowRadius = 2
        backgroundLayer2.shadowOpacity = 0.8
        backgroundLayer2.masksToBounds = false
        backgroundLayer2.shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: 50).cgPath
        backgroundLayer2.cornerRadius = 50
    }
}
