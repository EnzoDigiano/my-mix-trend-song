//
//  FavoriteService.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import RealmSwift

/// Manage `Favorite` object to be stored into realm
class FavoriteService {
    
    fileprivate let dataProvider = DataProvider()
    
    /// Store a favorite object into realm
    /// - Parameter favorite: `Favorite` object
    func store(_ favorite: Favorite) {
        dataProvider.add(favorite)
    }
    
    /// Get all `Favorite` objects stored in realm
    /// - Returns: `Favorite` object array
    func getAll() -> [Favorite] {
        guard let list = dataProvider.objects(Favorite.self)?.toArray(type: Favorite.self) else { return []}
        return list
    }
    
    /// Get a favorite object by id if exist
    /// - Parameter id: `Int` vale
    /// - Returns: `Favorite` object or `nil`
    func getById(_ id: String) -> Favorite? {
        let predicate = NSPredicate(format: "id == %@", id)
        return dataProvider.objects(Favorite.self,predicate: predicate)?.first
    }
    
    /// Get yes if a song ascociated with an id, is favorite
    /// - Parameter id: `String` value
    /// - Returns: `Bool` value
    func isFavorite(with id: String) -> Bool {
        let predicate = NSPredicate(format: "id == %@", id)
        return (dataProvider.objects(Favorite.self, predicate: predicate)?.first != nil)
    }
    
    /// Remove a favorite object from realm local storage
    /// - Parameter id: `String` value
    func delete( _ id: String) {
        guard let hasRemove = self.getById(id) else { return }
        dataProvider.delete(hasRemove)
    }
}
