//
//  DataProvider.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Realm
import RealmSwift

/// It is responsible for managing connections with Realm storage in current device
struct DataProvider {
    
    /// Validation to use Realm
    ///
    /// - Returns: Return a `Bool` status
    func isRealmAccessible() -> Bool {
        return (self.getURL() != nil)
    }
    
    func isInWriteTransaction() -> Bool {
        return self.realm.isInWriteTransaction
    }
    
    func beginWrite() -> () {
        return self.realm.beginWrite()
    }
    
    /// Configure the Realm database
    func configureRealm() {
        
        let config = Realm.Configuration(
            schemaVersion: 1,
            //swiftlint:disable:next unused_closure_parameter
            migrationBlock: { migration, oldSchemaVersion in
                if (oldSchemaVersion < 1) {
                    // Nothing to do!
                    // Realm will automatically detect new properties and removed properties
                    // And will update the schema on disk automatically
                }
            }
           ,deleteRealmIfMigrationNeeded: true
        )
        
        Realm.Configuration.defaultConfiguration = config
        print("Realm \(Realm.Configuration.defaultConfiguration.fileURL!)")
    }
    
    func getURL() -> URL?{
        return realm.configuration.fileURL
    }
    
    private var realm: Realm {
        var config = Realm.Configuration()
        config.shouldCompactOnLaunch = { totalBytes, usedBytes in
            // totalBytes refers to the size of the file on disk in bytes (data + free space)
            // usedBytes refers to the number of bytes used by data in the file

            // Compact if the file is over 100MB in size and less than 50% 'used'
            let oneHundredMB = 100 * 1024 * 1024
            return (totalBytes > oneHundredMB) && (Double(usedBytes) / Double(totalBytes)) < 0.5
        }
        // Set a custom file name
        config.fileURL = config.fileURL!.deletingLastPathComponent().appendingPathComponent("MyMixTrendSong.realm")
        // Get our Realm file's parent directory
        let folderPath = config.fileURL!.deletingLastPathComponent().path
        // Disable file protection for this directory
        try! FileManager.default.setAttributes([FileAttributeKey(rawValue: FileAttributeKey.protectionKey.rawValue): FileProtectionType.none], ofItemAtPath: folderPath)
        // Apply Configuration
        return try! Realm(configuration: config)
    }
}

extension DataProvider {
    
    /// Get an object collection associated to an object type and a predicate
    ///
    /// - Parameters:
    ///   - type: `Object` type
    ///   - predicate: ´NSPredicate´ objecgt or ´nil´
    /// - Returns: return an object collection or ´nil´
    func objects<T: Object>(_ type: T.Type, predicate: NSPredicate? = nil) -> Results<T>? {
        if !isRealmAccessible() { return nil }
        
        return predicate == nil ? realm.objects(type) : realm.objects(type).filter(predicate!)
    }
    
    /// Get an object associated to an object type and an identifier
    ///
    /// - Parameters:
    ///   - type: `Object` type
    ///   - key: `String` identifier
    /// - Returns: return an `Object` or `nil`
    func object<T: Object>(_ type: T.Type, key: String) -> T? {
        if !isRealmAccessible() { return nil }
        
        return realm.object(ofType: type, forPrimaryKey: key)
    }
    
    /// Get an object associated to an object type and an identifier
    ///
    /// - Parameters:
    ///   - type: `Object` type
    ///   - key: `Int` identifier
    /// - Returns: return an `Object` or `nil`
    func object<T: Object>(_ type: T.Type, id: Int) -> T? {
        if !isRealmAccessible() { return nil }
        
        return realm.object(ofType: type, forPrimaryKey: id)
    }
    
    /// Save into Realm an Object collection
    ///
    /// - Parameters:
    ///   - data: `Object` array
    ///   - update: `Bool` state, default `true`
    func add<T: Object>(_ data: [T], update: Realm.UpdatePolicy = .modified) {
        if !isRealmAccessible() { return }
        
        if realm.isInWriteTransaction {
            realm.add(data, update: update)
        } else {
            try? realm.write {
                realm.add(data, update: update)
            }
        }
    }
    
    /// Save into Realm a single Object
    ///
    /// - Parameters:
    ///   - data: `Object`
    ///   - update: `Bool` state, default `true`
    func add<T: Object>(_ data: T, update: Realm.UpdatePolicy = .modified) {
        add([data], update: update)
    }
    
    /// Run any action in realm database
    ///
    /// - Parameter action: Closure object
    func runTransaction(action: () -> Void) {
        if !isRealmAccessible() { return }
        
        try? realm.write {
            action()
        }
    }
    
    /// Delete an object collection from Realm database
    ///
    /// - Parameter data: `Object` collection
    func delete<T: Object>(_ data: [T]) {
        try? realm.write { realm.delete(data) }
    }
    
    /// Delete a single object from Realm database
    ///
    /// - Parameter data: `Object`
    func delete<T: Object>(_ data: T) {
        delete([data])
    }
    
    /// Delete all table from realm
    /// - Parameter data: `Object` types  collection
    func deleteAllByType<T: Object>(_ data: [T.Type]) {
        try? realm.write {
            for object in data {
                let allObjects = realm.objects(object)
                realm.delete(allObjects)
            }
        }
    }
    
    /// Delete all tables data from Realm Database
    func clearAllData() {
        if !isRealmAccessible() { return }
        try? realm.write { realm.deleteAll() }
    }
    
    func reference() -> Realm {
        return realm
    }
}
