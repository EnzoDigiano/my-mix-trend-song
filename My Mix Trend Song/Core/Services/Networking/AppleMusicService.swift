//
//  AppleMusicService.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class AppleMusicService {

    let developerToken = "eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IldlYlBsYXlLaWQifQ.eyJpc3MiOiJBTVBXZWJQbGF5IiwiaWF0IjoxNTk1NjEzMjkxLCJleHAiOjE2MTExNjUyOTF9.x4XfyeGTI7ukTJeAS_JU1cojk9fjmBzlXU8xMLTJGA2SbTyuzCYp-SQqAoAfKNNbOUvLQIl552l2tonoSU92LQ"

    func fetchHotTracks() -> Promise<HotTracksResponse> {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(developerToken)",
            "Accept": "application/json"
        ]
        return Promise { seal in
            let request = AF.request("https://amp-api.music.apple.com/v1/editorial/us/rooms/1457265758", headers: headers)
            request.responseDecodable(of: HotTracksResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
    
    func GetHint(search: String) -> Promise<HintResponse> {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(developerToken)",
            "Accept": "application/json"
        ]
        
        let termQueryItem = URLQueryItem(name: "term", value: search)
        let typesQueryItem = URLQueryItem(name: "types", value: "activities,artists,apple-curators,albums,curators,songs,playlists,music-videos,stations,tv-episodes,music-movies")
        let limitQueryItem = URLQueryItem(name: "limit", value: "10")

        var component = URLComponents()
        component.scheme = "https"
        component.host = "amp-api.music.apple.com"
        component.path = "/v1/catalog/us/search/hints"
        component.queryItems = [
            termQueryItem,
            typesQueryItem,
            limitQueryItem
        ]
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: HintResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
    
    func SearchSong(search: String) -> Promise<SearchSongsResponse> {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(developerToken)",
            "Accept": "application/json"
        ]
        
        let termQueryItem = URLQueryItem(name: "term", value: search)
        let typesQueryItem = URLQueryItem(name: "types", value: "activities,artists,apple-curators,albums,curators,songs,playlists,music-videos,stations,tv-episodes,music-movies")
        let limitQueryItem = URLQueryItem(name: "limit", value: "25")
        let includeQueryItem = URLQueryItem(name: "includeOnly", value: "artists")
        let extendQueryItem = URLQueryItem(name: "extend", value: "artistUrl")
        let withQueryItem = URLQueryItem(name: "with", value: "serverBubbles")
        
        var component = URLComponents()
        component.scheme = "https"
        component.host = "amp-api.music.apple.com"
        component.path = "/v1/catalog/us/search"
        component.queryItems = [
            termQueryItem,
            typesQueryItem,
            limitQueryItem,
            includeQueryItem,
            extendQueryItem,
            withQueryItem
        ]
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: SearchSongsResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
    
    func getGroupings() -> Promise<GroupingsResponse> {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(developerToken)",
            "Accept": "application/json"
        ]
        
        let platformQueryItem = URLQueryItem(name: "platform", value: "desktop")
        let nameQueryItem = URLQueryItem(name: "name", value: "music")
        let lQueryItem = URLQueryItem(name: "l", value: "en-us")
        let extendQueryItem = URLQueryItem(name: "extend", value: "editorialArtwork,artistUrl")
        
        var component = URLComponents()
        component.scheme = "https"
        component.host = "amp-api.music.apple.com"
        component.path = "/v1/editorial/us/groupings"
        component.queryItems = [
            platformQueryItem,
            nameQueryItem,
            lQueryItem,
            extendQueryItem
        ]
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: GroupingsResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
    
    func getArtist(with id: String) -> Promise<ArtistResponse> {
        let headers: HTTPHeaders = [
            "Authorization": "Bearer \(developerToken)",
            "Accept": "application/json"
        ]
        
        let includeQueryItem = URLQueryItem(name: "include", value: "songs,albums,genres,music-videos,playlists,station")
        let viewsQueryItem = URLQueryItem(name: "views", value: "featured-release,full-albums,appears-on-albums,featured-albums,featured-on-albums,singles,compilation-albums,live-albums,latest-release,top-music-videos,similar-artists,top-songs,playlists")
        let extendQueryItem = URLQueryItem(name: "extend", value: "artistBio,bornOrFormed,editorialArtwork,origin")
//        let extendPlaylistQueryItem = URLQueryItem(name: "extend[playlists]", value: "trackCount")
        let lQueryItem = URLQueryItem(name: "l", value: "en-us")
        
        
        var component = URLComponents()
        component.scheme = "https"
        component.host = "amp-api.music.apple.com"
        component.path = "/v1/catalog/us/artists/\(id)"
        component.queryItems = [
            includeQueryItem,
            viewsQueryItem,
            extendQueryItem,
//            extendPlaylistQueryItem,
            lQueryItem
        ]
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: ArtistResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
}
