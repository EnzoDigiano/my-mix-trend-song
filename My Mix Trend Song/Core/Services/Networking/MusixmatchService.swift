//
//  MusixmatchService.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 03/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import Alamofire
import PromiseKit

class MusixmatchService {

    let APIKey = "44ca2252485bf8a97c3f47a7c1b20439"
    
    func GetLyric(artist: String, track: String) -> Promise<LyricResponse> {
        let headers: HTTPHeaders = ["Accept": "application/json"]
        
        let formatQueryItem = URLQueryItem(name: "format", value: "json")
        let apikeyQueryItem = URLQueryItem(name: "apikey", value: APIKey)
        let artistQueryItem = URLQueryItem(name: "q_artist", value: artist)
        let trackQueryItem = URLQueryItem(name: "q_track", value: track)

        var component = URLComponents()
        component.scheme = "https"
        component.host = "api.musixmatch.com"
        component.path = "/ws/1.1/matcher.lyrics.get"
        component.queryItems = [formatQueryItem,
                                apikeyQueryItem,
                                artistQueryItem,
                                trackQueryItem]
        
        return Promise { seal in
            let request = AF.request(component.url!, headers: headers)
            request.responseDecodable(of: LyricResponse.self){ response in
                switch response.result {
                case .success(let value):
                    seal.fulfill(value)
                case .failure(let error):
                    print(error.localizedDescription)
                    seal.reject(error)
                }
            }
        }
    }
}
