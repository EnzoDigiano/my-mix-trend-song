//
//  UITableViewCell.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 06/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    func setDisclosure(toColour: UIColor) -> () {
        for view in self.subviews {
            if let disclosure = view as? UIButton {
                if let image = disclosure.backgroundImage(for: .normal) {
                    let colouredImage = image.withRenderingMode(.alwaysTemplate)
                    disclosure.setImage(colouredImage, for: .normal)
                    disclosure.tintColor = toColour
                }
            }
        }
    }
}
