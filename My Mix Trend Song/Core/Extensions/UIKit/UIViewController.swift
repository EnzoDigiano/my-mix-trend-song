//
//  UIViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
#if DEBUG
import SwiftUI

@available(iOS 13, *)
extension UIViewController {
    public  struct Preview: UIViewControllerRepresentable {
        // this variable is used for injecting the current view controller
        let viewController: UIViewController

        public func makeUIViewController(context: Context) -> UIViewController {
            return viewController
        }

        public func updateUIViewController(_ uiViewController: UIViewController, context: Context) {
        }
    }

    public func toPreview() -> some View {
        // inject self (the current view controller) for the preview
        Preview(viewController: self)
    }
}
#endif

fileprivate var aView: UIView?

extension UIViewController {
    
    func showSpinner(){
        aView = UIView(frame: self.view.bounds)
        aView!.backgroundColor = UIColor(displayP3Red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.center = aView!.center
        activityIndicator.startAnimating()
        aView!.addSubview(activityIndicator)
        self.view.addSubview(aView!)
        Timer.scheduledTimer(withTimeInterval: 20, repeats: false) { (_) in
            self.stopSpinner()
        }
    }
    
    func stopSpinner(){
        aView?.removeFromSuperview()
        aView = nil
    }
    
    func shouldDismissKeyboard() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(disdismissKeyboard(_:)))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc private func disdismissKeyboard(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func hideBackButton() {
        self.navigationItem.setHidesBackButton(true, animated: false)
    }
}
