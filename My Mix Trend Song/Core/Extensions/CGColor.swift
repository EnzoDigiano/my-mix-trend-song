//
//  CGColor.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 04/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import UIKit

extension CGColor {
    
    var UIColor: UIKit.UIColor {
        return UIKit.UIColor(cgColor: self)
    }
}
