//
//  AnalyticsManager.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAnalytics

class AnalyticsManager {
    
    static let shared = AnalyticsManager()
    
    enum Event: String {
        case applicationInstall     = "App Install"
        case applicationUninstall   = "App Unistall"
        case play                   = "Play song"
        case favorite               = "Favorite"
        case share                  = "Share"
        case search                 = "Search"
        case buy                    = "Buy" 
    }
    
    func track(event: Event, params: [String : Any]? = nil) {
        Analytics.logEvent(event.rawValue, parameters: params)
    }
}
