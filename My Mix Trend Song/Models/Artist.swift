//
//  Artist.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 07/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import RealmSwift

class Artist: MusicItem {
    
    @objc dynamic var bornOrFormed: String = ""
    @objc dynamic var origin: String = ""
    @objc dynamic var artistBio: String = ""
    
    /// Convert a `Artists` object into `favorite`
    /// - Returns: `Favorite` object
    override func toFavorite() -> Favorite {
        let result = super.toFavorite()
        result.type = SearchBarScope.Artists.rawValue
        return result
    }
}

extension ArtistResponseDatum {
    
    /// Parse response into `Artist` object
    /// - Returns: `Artists` object
    func toArtist() -> Artist {
        let artist = Artist()
        artist.id = self.id ?? "0"
        artist.name = self.attributes?.name ?? "Artist Name"
        artist.cover = self.attributes?.artwork?.url ?? "Artist cover url"
        artist.genre = self.attributes?.genreNames?.first ?? "Artist genre"
        
        artist.bornOrFormed = self.attributes?.bornOrFormed ?? "Artist born or formed"
        artist.origin = self.attributes?.origin ?? "Artist origin"
        artist.artistBio = self.attributes?.artistBio ?? "Artist bio"
        
        artist.descriptions = "\(artist.origin) - \(artist.bornOrFormed)"
        return artist
    }
}

extension SearchSongsArtistsDatum {
    
    /// Parse response into `Artist` object
    /// - Returns: `Artists` object
    func toArtist() -> Artist{
        let artist = Artist()
        artist.id = self.id ?? "0"
        artist.name = self.attributes?.name ?? "Song Name"
        artist.cover = self.attributes?.artwork?.url ?? ""
        artist.genre = self.attributes?.genreNames?.first ?? ""
        
        artist.bornOrFormed = ""
        artist.origin = ""
        artist.artistBio =  ""
        
        artist.descriptions = "\(artist.genre)"
        return artist
    }
}
