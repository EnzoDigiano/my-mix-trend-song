//
//  Albun.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 12/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

class Albun: MusicItem {
    
    @objc dynamic var artistName: String = ""
    @objc dynamic var copyright: String = ""
    
    /// Convert a `Albun` object into `favorite`
    /// - Returns: `Favorite` object
    override func toFavorite() -> Favorite {
        let result = super.toFavorite()
        result.type = SearchBarScope.Albums.rawValue
        return result
    }
}

extension SearchSongsAlbumDatum {
    
    func toAlbun() -> Albun{
        let albun = Albun()
        albun.id = self.attributes?.playParams?.id ?? "0"
        albun.name = self.attributes?.name ?? "Song Name"
        albun.cover = self.attributes?.artwork?.url ?? ""
        albun.genre = self.attributes?.genreNames?.first ?? ""
        
        albun.artistName = self.attributes?.recordLabel ?? "Record Label"
        albun.copyright = self.attributes?.copyright ?? "Copyright"
        
        albun.descriptions = "\(albun.artistName) - \(albun.copyright)"
        return albun
    }
}


