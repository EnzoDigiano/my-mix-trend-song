//
//  Song.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import RealmSwift

class Song: MusicItem {

    @objc dynamic var songName: String = ""
    @objc dynamic var albumName: String = ""
    @objc dynamic var artistName: String = ""
    @objc dynamic var preview: String = ""

    /// Convert a `song` object into `favorite`
    /// - Returns: `Favorite` object
    override func toFavorite() -> Favorite {
        let result = super.toFavorite()
        result.type = SearchBarScope.Songs.rawValue
        return result
    }
    
}

extension HotTracksContentsDatum {
    
    func toSong() -> Song{
        let song = Song()
        song.id = self.attributes?.playParams?.id ?? "0"
        song.name = self.attributes?.name ?? "Name"
        song.cover = self.attributes?.artwork?.url ?? ""
        song.genre = self.attributes?.genreNames?.first ?? ""
        
        song.songName = self.attributes?.name ?? "Song Name"
        song.albumName = self.attributes?.albumName ?? "Albune Name"
        song.artistName = self.attributes?.artistName ?? "Artist Name"
        song.preview = self.attributes?.previews?.first?.url ?? ""
        
        song.descriptions = "\(song.albumName) - \(song.artistName)"
        return song
    }
}

extension SearchSongsSongDatum {
    
    func toSong() -> Song{
        let song = Song()
        song.id = self.attributes?.playParams?.id ?? "0"
        song.name = self.attributes?.name ?? "Name"
        song.cover = self.attributes?.artwork?.url ?? ""
        song.genre = self.attributes?.genreNames?.first ?? ""
        
        song.songName = self.attributes?.name ?? "Song Name"
        song.albumName = self.attributes?.albumName ?? "Albune Name"
        song.artistName = self.attributes?.artistName ?? "Artist Name"
        song.preview = self.attributes?.previews?.first?.url ?? ""
        
        song.descriptions = "\(song.albumName) - \(song.artistName)"
        return song
    }
}
