//
//  HintResponse.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 06/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - HintResponse
struct HintResponse: Codable {
    var results: HintResults?

    enum CodingKeys: String, CodingKey {
        case results = "results"
    }
}

// MARK: - HintResults
struct HintResults: Codable {
    var terms: [String]?

    enum CodingKeys: String, CodingKey {
        case terms = "terms"
    }
}
