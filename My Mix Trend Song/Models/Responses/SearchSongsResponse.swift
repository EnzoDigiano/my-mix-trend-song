//
//  SearchSongsResponse.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 06/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - SearchSongsResponse
struct SearchSongsResponse: Codable {
    var results: SearchSongsResponseResults?
    var meta: SearchSongsMeta?

    enum CodingKeys: String, CodingKey {
        case results = "results"
        case meta = "meta"
    }
}

// MARK: - SearchSongsMeta
struct SearchSongsMeta: Codable {
    var results: SearchSongsMetaResults?
    var metrics: SearchSongsMetrics?

    enum CodingKeys: String, CodingKey {
        case results = "results"
        case metrics = "metrics"
    }
}

// MARK: - SearchSongsMetrics
struct SearchSongsMetrics: Codable {
    var dataSetId: String?

    enum CodingKeys: String, CodingKey {
        case dataSetId = "dataSetId"
    }
}

// MARK: - SearchSongsMetaResults
struct SearchSongsMetaResults: Codable {
    var order: [String]?

    enum CodingKeys: String, CodingKey {
        case order = "order"
    }
}

// MARK: - SearchSongsResponseResults
struct SearchSongsResponseResults: Codable {
    var top: SearchSongsTop?
    var artist: SearchSongsArtist?
    var album: SearchSongsAlbum?
    var song: SearchSongsSong?
    var playlist: SearchSongsPlaylist?
    var radioShow: SearchSongsCategory?
    var radioEpisode: SearchSongsCategory?
    var station: SearchSongsCategory?
    var musicVideo: SearchSongsMusicVideo?
    var videoExtra: SearchSongsCategory?
    var category: SearchSongsCategory?
    var curator: SearchSongsCategory?

    enum CodingKeys: String, CodingKey {
        case top = "top"
        case artist = "artist"
        case album = "album"
        case song = "song"
        case playlist = "playlist"
        case radioShow = "radio_show"
        case radioEpisode = "radio_episode"
        case station = "station"
        case musicVideo = "music_video"
        case videoExtra = "video_extra"
        case category = "category"
        case curator = "curator"
    }
}

// MARK: - SearchSongsAlbum
struct SearchSongsAlbum: Codable {
    var href: String?
    var next: String?
    var name: String?
    var groupId: String?
    var data: [SearchSongsAlbumDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case next = "next"
        case name = "name"
        case groupId = "groupId"
        case data = "data"
    }
}

// MARK: - SearchSongsAlbumDatum
struct SearchSongsAlbumDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: SearchSongsPurpleAttributes?
    var relationships: SearchSongsRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - SearchSongsPurpleAttributes
struct SearchSongsPurpleAttributes: Codable {
    var artwork: SearchSongsArtwork?
    var artistName: String?
    var isSingle: Bool?
    var url: String?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var name: String?
    var artistUrl: String?
    var recordLabel: String?
    var copyright: String?
    var playParams: SearchSongsPlayParams?
    var isCompilation: Bool?
    var contentRating: String?
    var editorialNotes: SearchSongsEditorialNotes?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case url = "url"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case name = "name"
        case artistUrl = "artistUrl"
        case recordLabel = "recordLabel"
        case copyright = "copyright"
        case playParams = "playParams"
        case isCompilation = "isCompilation"
        case contentRating = "contentRating"
        case editorialNotes = "editorialNotes"
    }
}

// MARK: - SearchSongsArtwork
struct SearchSongsArtwork: Codable {
    var width: Int?
    var height: Int?
    var url: String?
    var bgColor: String?
    var textColor1: String?
    var textColor2: String?
    var textColor3: String?
    var textColor4: String?

    enum CodingKeys: String, CodingKey {
        case width = "width"
        case height = "height"
        case url = "url"
        case bgColor = "bgColor"
        case textColor1 = "textColor1"
        case textColor2 = "textColor2"
        case textColor3 = "textColor3"
        case textColor4 = "textColor4"
    }
}

// MARK: - SearchSongsEditorialNotes
struct SearchSongsEditorialNotes: Codable {
    var standard: String?
    var short: String?

    enum CodingKeys: String, CodingKey {
        case standard = "standard"
        case short = "short"
    }
}

// MARK: - SearchSongsPlayParams
struct SearchSongsPlayParams: Codable {
    var id: String?
    var kind: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case kind = "kind"
    }
}

// MARK: - SearchSongsRelationships
struct SearchSongsRelationships: Codable {
    var artists: SearchSongsArtists?

    enum CodingKeys: String, CodingKey {
        case artists = "artists"
    }
}

// MARK: - SearchSongsArtists
struct SearchSongsArtists: Codable {
    var href: String?
    var data: [SearchSongsArtistsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - SearchSongsArtistsDatum
struct SearchSongsArtistsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: SearchSongsFluffyAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - SearchSongsFluffyAttributes
struct SearchSongsFluffyAttributes: Codable {
    var name: String?
    var url: String?
    var artwork: SearchSongsArtwork?
    var genreNames: [String]?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case url = "url"
        case artwork = "artwork"
        case genreNames = "genreNames"
    }
}

// MARK: - SearchSongsArtist
struct SearchSongsArtist: Codable {
    var href: String?
    var name: String?
    var groupId: String?
    var data: [SearchSongsArtistsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case name = "name"
        case groupId = "groupId"
        case data = "data"
    }
}

// MARK: - SearchSongsCategory
struct SearchSongsCategory: Codable {
    var href: String?
    var name: String?
    var groupId: String?
    var data: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case name = "name"
        case groupId = "groupId"
        case data = "data"
    }
}

// MARK: - SearchSongsMusicVideo
struct SearchSongsMusicVideo: Codable {
    var href: String?
    var name: String?
    var groupId: String?
    var data: [SearchSongsMusicVideoDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case name = "name"
        case groupId = "groupId"
        case data = "data"
    }
}

// MARK: - SearchSongsMusicVideoDatum
struct SearchSongsMusicVideoDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: SearchSongsTentacledAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - SearchSongsTentacledAttributes
struct SearchSongsTentacledAttributes: Codable {
    var previews: [SearchSongsPurplePreview]?
    var artwork: SearchSongsArtwork?
    var artistName: String?
    var url: String?
    var genreNames: [String]?
    var has4K: Bool?
    var durationInMillis: Int?
    var releaseDate: String?
    var name: String?
    var artistUrl: String?
    var isrc: String?
    var playParams: SearchSongsPlayParams?
    var hasHdr: Bool?
    var contentRating: String?

    enum CodingKeys: String, CodingKey {
        case previews = "previews"
        case artwork = "artwork"
        case artistName = "artistName"
        case url = "url"
        case genreNames = "genreNames"
        case has4K = "has4K"
        case durationInMillis = "durationInMillis"
        case releaseDate = "releaseDate"
        case name = "name"
        case artistUrl = "artistUrl"
        case isrc = "isrc"
        case playParams = "playParams"
        case hasHdr = "hasHDR"
        case contentRating = "contentRating"
    }
}

// MARK: - SearchSongsPurplePreview
struct SearchSongsPurplePreview: Codable {
    var url: String?
    var hlsUrl: String?
    var artwork: SearchSongsArtwork?

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case hlsUrl = "hlsUrl"
        case artwork = "artwork"
    }
}

// MARK: - SearchSongsPlaylist
struct SearchSongsPlaylist: Codable {
    var href: String?
    var name: String?
    var groupId: String?
    var data: [SearchSongsPlaylistDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case name = "name"
        case groupId = "groupId"
        case data = "data"
    }
}

// MARK: - SearchSongsPlaylistDatum
struct SearchSongsPlaylistDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: SearchSongsStickyAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - SearchSongsStickyAttributes
struct SearchSongsStickyAttributes: Codable {
    var artwork: SearchSongsArtwork?
    var isChart: Bool?
    var url: String?
    var lastModifiedDate: String?
    var name: String?
    var playlistType: String?
    var curatorName: String?
    var playParams: SearchSongsPlayParams?
    var attributesDescription: SearchSongsEditorialNotes?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case isChart = "isChart"
        case url = "url"
        case lastModifiedDate = "lastModifiedDate"
        case name = "name"
        case playlistType = "playlistType"
        case curatorName = "curatorName"
        case playParams = "playParams"
        case attributesDescription = "description"
    }
}

// MARK: - SearchSongsSong
struct SearchSongsSong: Codable {
    var href: String?
    var next: String?
    var name: String?
    var groupId: String?
    var data: [SearchSongsSongDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case next = "next"
        case name = "name"
        case groupId = "groupId"
        case data = "data"
    }
}

// MARK: - SearchSongsSongDatum
struct SearchSongsSongDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: SearchSongsIndigoAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - SearchSongsIndigoAttributes
struct SearchSongsIndigoAttributes: Codable {
    var previews: [SearchSongsFluffyPreview]?
    var artwork: SearchSongsArtwork?
    var artistName: String?
    var url: String?
    var discNumber: Int?
    var genreNames: [String]?
    var hasTimeSyncedLyrics: Bool?
    var durationInMillis: Int?
    var releaseDate: String?
    var name: String?
    var artistUrl: String?
    var isrc: String?
    var hasLyrics: Bool?
    var albumName: String?
    var playParams: SearchSongsPlayParams?
    var trackNumber: Int?
    var contentRating: String?
    var composerName: String?

    enum CodingKeys: String, CodingKey {
        case previews = "previews"
        case artwork = "artwork"
        case artistName = "artistName"
        case url = "url"
        case discNumber = "discNumber"
        case genreNames = "genreNames"
        case hasTimeSyncedLyrics = "hasTimeSyncedLyrics"
        case durationInMillis = "durationInMillis"
        case releaseDate = "releaseDate"
        case name = "name"
        case artistUrl = "artistUrl"
        case isrc = "isrc"
        case hasLyrics = "hasLyrics"
        case albumName = "albumName"
        case playParams = "playParams"
        case trackNumber = "trackNumber"
        case contentRating = "contentRating"
        case composerName = "composerName"
    }
}

// MARK: - SearchSongsFluffyPreview
struct SearchSongsFluffyPreview: Codable {
    var url: String?

    enum CodingKeys: String, CodingKey {
        case url = "url"
    }
}

// MARK: - SearchSongsTop
struct SearchSongsTop: Codable {
    var name: String?
    var groupId: String?
    var data: [SearchSongsTopDatum]?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case groupId = "groupId"
        case data = "data"
    }
}

// MARK: - SearchSongsTopDatum
struct SearchSongsTopDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: SearchSongsIndecentAttributes?
    var relationships: SearchSongsRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - SearchSongsIndecentAttributes
struct SearchSongsIndecentAttributes: Codable {
    var name: String?
    var url: String?
    var artwork: SearchSongsArtwork?
    var artistName: String?
    var isSingle: Bool?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var artistUrl: String?
    var recordLabel: String?
    var copyright: String?
    var playParams: SearchSongsPlayParams?
    var isCompilation: Bool?
    var contentRating: String?
    var previews: [SearchSongsFluffyPreview]?
    var discNumber: Int?
    var hasTimeSyncedLyrics: Bool?
    var durationInMillis: Int?
    var isrc: String?
    var hasLyrics: Bool?
    var albumName: String?
    var trackNumber: Int?
    var isChart: Bool?
    var lastModifiedDate: String?
    var playlistType: String?
    var curatorName: String?
    var attributesDescription: SearchSongsEditorialNotes?
    var composerName: String?
    var editorialNotes: SearchSongsEditorialNotes?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case url = "url"
        case artwork = "artwork"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case artistUrl = "artistUrl"
        case recordLabel = "recordLabel"
        case copyright = "copyright"
        case playParams = "playParams"
        case isCompilation = "isCompilation"
        case contentRating = "contentRating"
        case previews = "previews"
        case discNumber = "discNumber"
        case hasTimeSyncedLyrics = "hasTimeSyncedLyrics"
        case durationInMillis = "durationInMillis"
        case isrc = "isrc"
        case hasLyrics = "hasLyrics"
        case albumName = "albumName"
        case trackNumber = "trackNumber"
        case isChart = "isChart"
        case lastModifiedDate = "lastModifiedDate"
        case playlistType = "playlistType"
        case curatorName = "curatorName"
        case attributesDescription = "description"
        case composerName = "composerName"
        case editorialNotes = "editorialNotes"
    }
}

// MARK: - Encode/decode helpers

class JSONNull: Codable, Hashable {

    public static func == (lhs: JSONNull, rhs: JSONNull) -> Bool {
        return true
    }

    public var hashValue: Int {
        return 0
    }

    public func hash(into hasher: inout Hasher) {
        // No-op
    }

    public init() {}

    public required init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        if !container.decodeNil() {
            throw DecodingError.typeMismatch(JSONNull.self, DecodingError.Context(codingPath: decoder.codingPath, debugDescription: "Wrong type for JSONNull"))
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        try container.encodeNil()
    }
}

class JSONCodingKey: CodingKey {
    let key: String

    required init?(intValue: Int) {
        return nil
    }

    required init?(stringValue: String) {
        key = stringValue
    }

    var intValue: Int? {
        return nil
    }

    var stringValue: String {
        return key
    }
}

class JSONAny: Codable {

    let value: Any

    static func decodingError(forCodingPath codingPath: [CodingKey]) -> DecodingError {
        let context = DecodingError.Context(codingPath: codingPath, debugDescription: "Cannot decode JSONAny")
        return DecodingError.typeMismatch(JSONAny.self, context)
    }

    static func encodingError(forValue value: Any, codingPath: [CodingKey]) -> EncodingError {
        let context = EncodingError.Context(codingPath: codingPath, debugDescription: "Cannot encode JSONAny")
        return EncodingError.invalidValue(value, context)
    }

    static func decode(from container: SingleValueDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if container.decodeNil() {
            return JSONNull()
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout UnkeyedDecodingContainer) throws -> Any {
        if let value = try? container.decode(Bool.self) {
            return value
        }
        if let value = try? container.decode(Int64.self) {
            return value
        }
        if let value = try? container.decode(Double.self) {
            return value
        }
        if let value = try? container.decode(String.self) {
            return value
        }
        if let value = try? container.decodeNil() {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer() {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decode(from container: inout KeyedDecodingContainer<JSONCodingKey>, forKey key: JSONCodingKey) throws -> Any {
        if let value = try? container.decode(Bool.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Int64.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(Double.self, forKey: key) {
            return value
        }
        if let value = try? container.decode(String.self, forKey: key) {
            return value
        }
        if let value = try? container.decodeNil(forKey: key) {
            if value {
                return JSONNull()
            }
        }
        if var container = try? container.nestedUnkeyedContainer(forKey: key) {
            return try decodeArray(from: &container)
        }
        if var container = try? container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key) {
            return try decodeDictionary(from: &container)
        }
        throw decodingError(forCodingPath: container.codingPath)
    }

    static func decodeArray(from container: inout UnkeyedDecodingContainer) throws -> [Any] {
        var arr: [Any] = []
        while !container.isAtEnd {
            let value = try decode(from: &container)
            arr.append(value)
        }
        return arr
    }

    static func decodeDictionary(from container: inout KeyedDecodingContainer<JSONCodingKey>) throws -> [String: Any] {
        var dict = [String: Any]()
        for key in container.allKeys {
            let value = try decode(from: &container, forKey: key)
            dict[key.stringValue] = value
        }
        return dict
    }

    static func encode(to container: inout UnkeyedEncodingContainer, array: [Any]) throws {
        for value in array {
            if let value = value as? Bool {
                try container.encode(value)
            } else if let value = value as? Int64 {
                try container.encode(value)
            } else if let value = value as? Double {
                try container.encode(value)
            } else if let value = value as? String {
                try container.encode(value)
            } else if value is JSONNull {
                try container.encodeNil()
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer()
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout KeyedEncodingContainer<JSONCodingKey>, dictionary: [String: Any]) throws {
        for (key, value) in dictionary {
            let key = JSONCodingKey(stringValue: key)!
            if let value = value as? Bool {
                try container.encode(value, forKey: key)
            } else if let value = value as? Int64 {
                try container.encode(value, forKey: key)
            } else if let value = value as? Double {
                try container.encode(value, forKey: key)
            } else if let value = value as? String {
                try container.encode(value, forKey: key)
            } else if value is JSONNull {
                try container.encodeNil(forKey: key)
            } else if let value = value as? [Any] {
                var container = container.nestedUnkeyedContainer(forKey: key)
                try encode(to: &container, array: value)
            } else if let value = value as? [String: Any] {
                var container = container.nestedContainer(keyedBy: JSONCodingKey.self, forKey: key)
                try encode(to: &container, dictionary: value)
            } else {
                throw encodingError(forValue: value, codingPath: container.codingPath)
            }
        }
    }

    static func encode(to container: inout SingleValueEncodingContainer, value: Any) throws {
        if let value = value as? Bool {
            try container.encode(value)
        } else if let value = value as? Int64 {
            try container.encode(value)
        } else if let value = value as? Double {
            try container.encode(value)
        } else if let value = value as? String {
            try container.encode(value)
        } else if value is JSONNull {
            try container.encodeNil()
        } else {
            throw encodingError(forValue: value, codingPath: container.codingPath)
        }
    }

    public required init(from decoder: Decoder) throws {
        if var arrayContainer = try? decoder.unkeyedContainer() {
            self.value = try JSONAny.decodeArray(from: &arrayContainer)
        } else if var container = try? decoder.container(keyedBy: JSONCodingKey.self) {
            self.value = try JSONAny.decodeDictionary(from: &container)
        } else {
            let container = try decoder.singleValueContainer()
            self.value = try JSONAny.decode(from: container)
        }
    }

    public func encode(to encoder: Encoder) throws {
        if let arr = self.value as? [Any] {
            var container = encoder.unkeyedContainer()
            try JSONAny.encode(to: &container, array: arr)
        } else if let dict = self.value as? [String: Any] {
            var container = encoder.container(keyedBy: JSONCodingKey.self)
            try JSONAny.encode(to: &container, dictionary: dict)
        } else {
            var container = encoder.singleValueContainer()
            try JSONAny.encode(to: &container, value: self.value)
        }
    }
}
