//
//  LyricResponse.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 03/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - LyricResponse
struct LyricResponse: Codable {
    var message: LyricMessage?

    enum CodingKeys: String, CodingKey {
        case message = "message"
    }
}

// MARK: - LyricMessage
struct LyricMessage: Codable {
    var header: LyricHeader?
    var body: LyricBody?

    enum CodingKeys: String, CodingKey {
        case header = "header"
        case body = "body"
    }
}

// MARK: - LyricBody
struct LyricBody: Codable {
    var lyrics: Lyrics?

    enum CodingKeys: String, CodingKey {
        case lyrics = "lyrics"
    }
}

// MARK: - Lyrics
struct Lyrics: Codable {
    var lyricsId: Int?
    var explicit: Int?
    var lyricsBody: String?
    var scriptTrackingUrl: String?
    var pixelTrackingUrl: String?
    var lyricsCopyright: String?
    var updatedTime: String?

    enum CodingKeys: String, CodingKey {
        case lyricsId = "lyrics_id"
        case explicit = "explicit"
        case lyricsBody = "lyrics_body"
        case scriptTrackingUrl = "script_tracking_url"
        case pixelTrackingUrl = "pixel_tracking_url"
        case lyricsCopyright = "lyrics_copyright"
        case updatedTime = "updated_time"
    }
}

// MARK: - LyricHeader
struct LyricHeader: Codable {
    var statusCode: Int?
    var executeTime: Double?

    enum CodingKeys: String, CodingKey {
        case statusCode = "status_code"
        case executeTime = "execute_time"
    }
}
