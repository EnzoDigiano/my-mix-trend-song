//
//  Songs.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

// MARK: - HotTracksResponse
struct HotTracksResponse: Codable {
    var data: [HotTracksResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - HotTracksResponseDatum
struct HotTracksResponseDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: HotTracksPurpleAttributes?
    var relationships: HotTracksRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - HotTracksPurpleAttributes
struct HotTracksPurpleAttributes: Codable {
    var defaultSort: String?
    var title: String?
    var doNotFilter: Bool?
    var editorialElementKind: String?
    var sorts: [String]?
    var resourceTypes: [String]?

    enum CodingKeys: String, CodingKey {
        case defaultSort = "defaultSort"
        case title = "title"
        case doNotFilter = "doNotFilter"
        case editorialElementKind = "editorialElementKind"
        case sorts = "sorts"
        case resourceTypes = "resourceTypes"
    }
}

// MARK: - HotTracksRelationships
struct HotTracksRelationships: Codable {
    var contents: HotTracksContents?

    enum CodingKeys: String, CodingKey {
        case contents = "contents"
    }
}

// MARK: - HotTracksContents
struct HotTracksContents: Codable {
    var href: String?
    var data: [HotTracksContentsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - HotTracksContentsDatum
struct HotTracksContentsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: HotTracksFluffyAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - HotTracksFluffyAttributes
struct HotTracksFluffyAttributes: Codable {
    var previews: [HotTracksPreview]?
    var artwork: HotTracksArtwork?
    var artistName: String?
    var url: String?
    var discNumber: Int?
    var genreNames: [String]?
    var hasTimeSyncedLyrics: Bool?
    var durationInMillis: Int?
    var releaseDate: String?
    var name: String?
    var isrc: String?
    var hasLyrics: Bool?
    var albumName: String?
    var playParams: HotTracksPlayParams?
    var trackNumber: Int?
    var composerName: String?
    var contentRating: String?
    var attribution: String?

    enum CodingKeys: String, CodingKey {
        case previews = "previews"
        case artwork = "artwork"
        case artistName = "artistName"
        case url = "url"
        case discNumber = "discNumber"
        case genreNames = "genreNames"
        case hasTimeSyncedLyrics = "hasTimeSyncedLyrics"
        case durationInMillis = "durationInMillis"
        case releaseDate = "releaseDate"
        case name = "name"
        case isrc = "isrc"
        case hasLyrics = "hasLyrics"
        case albumName = "albumName"
        case playParams = "playParams"
        case trackNumber = "trackNumber"
        case composerName = "composerName"
        case contentRating = "contentRating"
        case attribution = "attribution"
    }
}

// MARK: - HotTracksArtwork
struct HotTracksArtwork: Codable {
    var width: Int?
    var height: Int?
    var url: String?
    var bgColor: String?
    var textColor1: String?
    var textColor2: String?
    var textColor3: String?
    var textColor4: String?

    enum CodingKeys: String, CodingKey {
        case width = "width"
        case height = "height"
        case url = "url"
        case bgColor = "bgColor"
        case textColor1 = "textColor1"
        case textColor2 = "textColor2"
        case textColor3 = "textColor3"
        case textColor4 = "textColor4"
    }
}

// MARK: - HotTracksPlayParams
struct HotTracksPlayParams: Codable {
    var id: String?
    var kind: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case kind = "kind"
    }
}

// MARK: - HotTracksPreview
struct HotTracksPreview: Codable {
    var url: String?

    enum CodingKeys: String, CodingKey {
        case url = "url"
    }
}
