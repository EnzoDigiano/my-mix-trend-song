//
//  GroupingsResponse.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 06/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - GroupingsResponse
struct GroupingsResponse: Codable {
    var data: [GroupingsResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - GroupingsResponseDatum
struct GroupingsResponseDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: GroupingsPurpleAttributes?
    var relationships: GroupingsPurpleRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - GroupingsPurpleAttributes
struct GroupingsPurpleAttributes: Codable {
    var name: String?
    var genreNames: [String]?

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case genreNames = "genreNames"
    }
}

// MARK: - GroupingsPurpleRelationships
struct GroupingsPurpleRelationships: Codable {
    var tabs: GroupingsTabs?

    enum CodingKeys: String, CodingKey {
        case tabs = "tabs"
    }
}

// MARK: - GroupingsTabs
struct GroupingsTabs: Codable {
    var href: String?
    var data: [GroupingsTabsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - GroupingsTabsDatum
struct GroupingsTabsDatum: Codable {
    var id: String?
    var type: String?
    var attributes: GroupingsFluffyAttributes?
    var relationships: GroupingsFluffyRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - GroupingsFluffyAttributes
struct GroupingsFluffyAttributes: Codable {
    var doNotFilter: Bool?
    var editorialElementKind: String?

    enum CodingKeys: String, CodingKey {
        case doNotFilter = "doNotFilter"
        case editorialElementKind = "editorialElementKind"
    }
}

// MARK: - GroupingsFluffyRelationships
struct GroupingsFluffyRelationships: Codable {
    var children: GroupingsPurpleChildren?

    enum CodingKeys: String, CodingKey {
        case children = "children"
    }
}

// MARK: - GroupingsPurpleChildren
struct GroupingsPurpleChildren: Codable {
    var data: [GroupingsPurpleDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - GroupingsPurpleDatum
struct GroupingsPurpleDatum: Codable {
    var id: String?
    var type: String?
    var attributes: GroupingsTentacledAttributes?
    var relationships: GroupingsTentacledRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - GroupingsTentacledAttributes
struct GroupingsTentacledAttributes: Codable {
    var displayStyle: String?
    var doNotFilter: Bool?
    var editorialElementKind: String?
    var links: [GroupingsLinkElement]?
    var name: String?
    var emphasize: Bool?
    var type: String?
    var featureFirstElement: Bool?

    enum CodingKeys: String, CodingKey {
        case displayStyle = "displayStyle"
        case doNotFilter = "doNotFilter"
        case editorialElementKind = "editorialElementKind"
        case links = "links"
        case name = "name"
        case emphasize = "emphasize"
        case type = "type"
        case featureFirstElement = "featureFirstElement"
    }
}

// MARK: - GroupingsLinkElement
struct GroupingsLinkElement: Codable {
    var label: String?
    var url: String?
    var target: String?

    enum CodingKeys: String, CodingKey {
        case label = "label"
        case url = "url"
        case target = "target"
    }
}

// MARK: - GroupingsTentacledRelationships
struct GroupingsTentacledRelationships: Codable {
    var children: GroupingsFluffyChildren?
    var room: GroupingsRoom?
    var contents: GroupingsFluffyContents?

    enum CodingKeys: String, CodingKey {
        case children = "children"
        case room = "room"
        case contents = "contents"
    }
}

// MARK: - GroupingsFluffyChildren
struct GroupingsFluffyChildren: Codable {
    var data: [GroupingsFluffyDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - GroupingsFluffyDatum
struct GroupingsFluffyDatum: Codable {
    var id: String?
    var type: String?
    var attributes: GroupingsStickyAttributes?
    var relationships: GroupingsStickyRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - GroupingsStickyAttributes
struct GroupingsStickyAttributes: Codable {
    var doNotFilter: Bool?
    var designBadge: String?
    var designTag: String?
    var artwork: GroupingsArtwork?
    var editorialElementKind: String?
    var link: GroupingsPurpleLink?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case doNotFilter = "doNotFilter"
        case designBadge = "designBadge"
        case designTag = "designTag"
        case artwork = "artwork"
        case editorialElementKind = "editorialElementKind"
        case link = "link"
        case name = "name"
    }
}

// MARK: - GroupingsArtwork
struct GroupingsArtwork: Codable {
    var width: Int?
    var height: Int?
    var url: String?
    var bgColor: String?
    var textColor1: String?
    var textColor2: String?
    var textColor3: String?
    var textColor4: String?

    enum CodingKeys: String, CodingKey {
        case width = "width"
        case height = "height"
        case url = "url"
        case bgColor = "bgColor"
        case textColor1 = "textColor1"
        case textColor2 = "textColor2"
        case textColor3 = "textColor3"
        case textColor4 = "textColor4"
    }
}

// MARK: - GroupingsPurpleLink
struct GroupingsPurpleLink: Codable {
    var label: String?
    var url: String?

    enum CodingKeys: String, CodingKey {
        case label = "label"
        case url = "url"
    }
}

// MARK: - GroupingsStickyRelationships
struct GroupingsStickyRelationships: Codable {
    var contents: GroupingsPurpleContents?

    enum CodingKeys: String, CodingKey {
        case contents = "contents"
    }
}

// MARK: - GroupingsPurpleContents
struct GroupingsPurpleContents: Codable {
    var data: [GroupingsTentacledDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - GroupingsTentacledDatum
struct GroupingsTentacledDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: GroupingsIndigoAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - GroupingsIndigoAttributes
struct GroupingsIndigoAttributes: Codable {
    var artwork: GroupingsArtwork?
    var isChart: Bool?
    var url: String?
    var lastModifiedDate: String?
    var name: String?
    var playlistType: String?
    var editorialArtwork: GroupingsEditorialArtwork?
    var curatorName: String?
    var playParams: GroupingsPurplePlayParams?
    var attributesDescription: GroupingsDescription?
    var artistName: String?
    var isSingle: Bool?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var artistUrl: String?
    var recordLabel: String?
    var copyright: String?
    var editorialNotes: GroupingsEditorialNotes?
    var isCompilation: Bool?
    var contentRating: String?
    var kind: String?
    var shortName: String?
    var previews: [GroupingsPurplePreview]?
    var discNumber: Int?
    var hasTimeSyncedLyrics: Bool?
    var durationInMillis: Int?
    var isrc: String?
    var hasLyrics: Bool?
    var albumName: String?
    var trackNumber: Int?
    var composerName: String?
    var has4K: Bool?
    var hasHdr: Bool?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case isChart = "isChart"
        case url = "url"
        case lastModifiedDate = "lastModifiedDate"
        case name = "name"
        case playlistType = "playlistType"
        case editorialArtwork = "editorialArtwork"
        case curatorName = "curatorName"
        case playParams = "playParams"
        case attributesDescription = "description"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case artistUrl = "artistUrl"
        case recordLabel = "recordLabel"
        case copyright = "copyright"
        case editorialNotes = "editorialNotes"
        case isCompilation = "isCompilation"
        case contentRating = "contentRating"
        case kind = "kind"
        case shortName = "shortName"
        case previews = "previews"
        case discNumber = "discNumber"
        case hasTimeSyncedLyrics = "hasTimeSyncedLyrics"
        case durationInMillis = "durationInMillis"
        case isrc = "isrc"
        case hasLyrics = "hasLyrics"
        case albumName = "albumName"
        case trackNumber = "trackNumber"
        case composerName = "composerName"
        case has4K = "has4K"
        case hasHdr = "hasHDR"
    }
}

// MARK: - GroupingsDescription
struct GroupingsDescription: Codable {
    var standard: String?
    var short: String?

    enum CodingKeys: String, CodingKey {
        case standard = "standard"
        case short = "short"
    }
}

// MARK: - GroupingsEditorialArtwork
struct GroupingsEditorialArtwork: Codable {
    var subscriptionHero: GroupingsArtwork?
    var subscriptionCover: GroupingsArtwork?
    var contentLogoTrimmed: GroupingsArtwork?
    var storeFlowcase: GroupingsArtwork?
    var bannerUber: GroupingsArtwork?
    var brandLogo: GroupingsArtwork?
    var emailFeature: GroupingsArtwork?

    enum CodingKeys: String, CodingKey {
        case subscriptionHero = "subscriptionHero"
        case subscriptionCover = "subscriptionCover"
        case contentLogoTrimmed = "contentLogoTrimmed"
        case storeFlowcase = "storeFlowcase"
        case bannerUber = "bannerUber"
        case brandLogo = "brandLogo"
        case emailFeature = "emailFeature"
    }
}

// MARK: - GroupingsEditorialNotes
struct GroupingsEditorialNotes: Codable {
    var standard: String?
    var short: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case standard = "standard"
        case short = "short"
        case name = "name"
    }
}

// MARK: - GroupingsPurplePlayParams
struct GroupingsPurplePlayParams: Codable {
    var id: String?
    var kind: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case kind = "kind"
    }
}

// MARK: - GroupingsPurplePreview
struct GroupingsPurplePreview: Codable {
    var url: String?
    var hlsUrl: String?
    var artwork: GroupingsArtwork?

    enum CodingKeys: String, CodingKey {
        case url = "url"
        case hlsUrl = "hlsUrl"
        case artwork = "artwork"
    }
}

// MARK: - GroupingsFluffyContents
struct GroupingsFluffyContents: Codable {
    var data: [GroupingsStickyDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - GroupingsStickyDatum
struct GroupingsStickyDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: GroupingsIndecentAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - GroupingsIndecentAttributes
struct GroupingsIndecentAttributes: Codable {
    var artwork: GroupingsArtwork?
    var isChart: Bool?
    var url: String?
    var lastModifiedDate: String?
    var name: String?
    var playlistType: String?
    var editorialArtwork: GroupingsEditorialArtwork?
    var curatorName: String?
    var playParams: GroupingsFluffyPlayParams?
    var attributesDescription: GroupingsDescription?
    var artistName: String?
    var isSingle: Bool?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var artistUrl: String?
    var recordLabel: String?
    var copyright: String?
    var editorialNotes: GroupingsEditorialNotes?
    var isCompilation: Bool?
    var contentRating: String?
    var previews: [GroupingsFluffyPreview]?
    var discNumber: Int?
    var hasTimeSyncedLyrics: Bool?
    var durationInMillis: Int?
    var isrc: String?
    var hasLyrics: Bool?
    var albumName: String?
    var trackNumber: Int?
    var composerName: String?
    var mediaKind: String?
    var requiresSubscription: Bool?
    var airTime: GroupingsAirTime?
    var streamingRadioSubType: String?
    var isLive: Bool?
    var episodeNumber: String?
    var postUrl: String?
    var uploadDate: String?
    var contentRatingsBySystem: GroupingsContentRatingsBySystem?
    var durationInMilliseconds: Int?
    var assetTokens: GroupingsAssetTokens?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case isChart = "isChart"
        case url = "url"
        case lastModifiedDate = "lastModifiedDate"
        case name = "name"
        case playlistType = "playlistType"
        case editorialArtwork = "editorialArtwork"
        case curatorName = "curatorName"
        case playParams = "playParams"
        case attributesDescription = "description"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case artistUrl = "artistUrl"
        case recordLabel = "recordLabel"
        case copyright = "copyright"
        case editorialNotes = "editorialNotes"
        case isCompilation = "isCompilation"
        case contentRating = "contentRating"
        case previews = "previews"
        case discNumber = "discNumber"
        case hasTimeSyncedLyrics = "hasTimeSyncedLyrics"
        case durationInMillis = "durationInMillis"
        case isrc = "isrc"
        case hasLyrics = "hasLyrics"
        case albumName = "albumName"
        case trackNumber = "trackNumber"
        case composerName = "composerName"
        case mediaKind = "mediaKind"
        case requiresSubscription = "requiresSubscription"
        case airTime = "airTime"
        case streamingRadioSubType = "streamingRadioSubType"
        case isLive = "isLive"
        case episodeNumber = "episodeNumber"
        case postUrl = "postUrl"
        case uploadDate = "uploadDate"
        case contentRatingsBySystem = "contentRatingsBySystem"
        case durationInMilliseconds = "durationInMilliseconds"
        case assetTokens = "assetTokens"
    }
}

// MARK: - GroupingsAirTime
struct GroupingsAirTime: Codable {
    var start: String?
    var end: String?

    enum CodingKeys: String, CodingKey {
        case start = "start"
        case end = "end"
    }
}

// MARK: - GroupingsAssetTokens
struct GroupingsAssetTokens: Codable {
    var sd480PVideo: String?
    var sdVideo: String?
    var sdVideoWithPlusAudio: String?
    var the1080PHdVideo: String?

    enum CodingKeys: String, CodingKey {
        case sd480PVideo = "sd480pVideo"
        case sdVideo = "sdVideo"
        case sdVideoWithPlusAudio = "sdVideoWithPlusAudio"
        case the1080PHdVideo = "1080pHdVideo"
    }
}

// MARK: - GroupingsContentRatingsBySystem
struct GroupingsContentRatingsBySystem: Codable {
}

// MARK: - GroupingsFluffyPlayParams
struct GroupingsFluffyPlayParams: Codable {
    var id: String?
    var kind: String?
    var format: String?
    var stationHash: String?
    var streamingKind: Int?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case kind = "kind"
        case format = "format"
        case stationHash = "stationHash"
        case streamingKind = "streamingKind"
    }
}

// MARK: - GroupingsFluffyPreview
struct GroupingsFluffyPreview: Codable {
    var url: String?

    enum CodingKeys: String, CodingKey {
        case url = "url"
    }
}

// MARK: - GroupingsRoom
struct GroupingsRoom: Codable {
    var data: [GroupingsRoomDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - GroupingsRoomDatum
struct GroupingsRoomDatum: Codable {
    var id: String?
    var type: String?
    var href: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
    }
}
