//
//  ArtistResponse.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 07/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - ArtistResponse
struct ArtistResponse: Codable {
    var data: [ArtistResponseDatum]?

    enum CodingKeys: String, CodingKey {
        case data = "data"
    }
}

// MARK: - ArtistResponseDatum
struct ArtistResponseDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistPurpleAttributes?
    var relationships: ArtistPurpleRelationships?
    var views: ArtistDatumViews?
    var meta: ArtistMeta?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
        case views = "views"
        case meta = "meta"
    }
}

// MARK: - ArtistPurpleAttributes
struct ArtistPurpleAttributes: Codable {
    var origin: String?
    var editorialArtwork: ArtistPurpleEditorialArtwork?
    var url: String?
    var artwork: ArtistArtwork?
    var genreNames: [String]?
    var bornOrFormed: String?
    var artistBio: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case origin = "origin"
        case editorialArtwork = "editorialArtwork"
        case url = "url"
        case artwork = "artwork"
        case genreNames = "genreNames"
        case bornOrFormed = "bornOrFormed"
        case artistBio = "artistBio"
        case name = "name"
    }
}

// MARK: - ArtistArtwork
struct ArtistArtwork: Codable {
    var width: Int?
    var height: Int?
    var url: String?
    var bgColor: String?
    var textColor1: String?
    var textColor2: String?
    var textColor3: String?
    var textColor4: String?

    enum CodingKeys: String, CodingKey {
        case width = "width"
        case height = "height"
        case url = "url"
        case bgColor = "bgColor"
        case textColor1 = "textColor1"
        case textColor2 = "textColor2"
        case textColor3 = "textColor3"
        case textColor4 = "textColor4"
    }
}

// MARK: - ArtistPurpleEditorialArtwork
struct ArtistPurpleEditorialArtwork: Codable {
}

// MARK: - ArtistMeta
struct ArtistMeta: Codable {
    var views: ArtistMetaViews?

    enum CodingKeys: String, CodingKey {
        case views = "views"
    }
}

// MARK: - ArtistMetaViews
struct ArtistMetaViews: Codable {
    var order: [String]?

    enum CodingKeys: String, CodingKey {
        case order = "order"
    }
}

// MARK: - ArtistPurpleRelationships
struct ArtistPurpleRelationships: Codable {
    var musicVideos: ArtistMusicVideos?
    var songs: ArtistSongs?
    var albums: ArtistPurpleAlbums?
    var station: ArtistFluffyStation?
    var genres: ArtistGenres?
    var playlists: ArtistRelationshipsPlaylists?

    enum CodingKeys: String, CodingKey {
        case musicVideos = "music-videos"
        case songs = "songs"
        case albums = "albums"
        case station = "station"
        case genres = "genres"
        case playlists = "playlists"
    }
}

// MARK: - ArtistPurpleAlbums
struct ArtistPurpleAlbums: Codable {
    var href: String?
    var next: String?
    var data: [ArtistFullAlbumsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case next = "next"
        case data = "data"
    }
}

// MARK: - ArtistFullAlbumsDatum
struct ArtistFullAlbumsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistFluffyAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistFluffyAttributes
struct ArtistFluffyAttributes: Codable {
    var artwork: ArtistArtwork?
    var artistName: String?
    var isSingle: Bool?
    var url: String?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var name: String?
    var recordLabel: String?
    var editorialArtwork: ArtistFluffyEditorialArtwork?
    var copyright: String?
    var playParams: ArtistPurplePlayParams?
    var isCompilation: Bool?
    var contentRating: String?
    var editorialNotes: ArtistEditorialNotes?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case url = "url"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case name = "name"
        case recordLabel = "recordLabel"
        case editorialArtwork = "editorialArtwork"
        case copyright = "copyright"
        case playParams = "playParams"
        case isCompilation = "isCompilation"
        case contentRating = "contentRating"
        case editorialNotes = "editorialNotes"
    }
}

// MARK: - ArtistFluffyEditorialArtwork
struct ArtistFluffyEditorialArtwork: Codable {
    var storeFlowcase: ArtistArtwork?
    var subscriptionHero: ArtistArtwork?

    enum CodingKeys: String, CodingKey {
        case storeFlowcase = "storeFlowcase"
        case subscriptionHero = "subscriptionHero"
    }
}

// MARK: - ArtistEditorialNotes
struct ArtistEditorialNotes: Codable {
    var standard: String?
    var short: String?

    enum CodingKeys: String, CodingKey {
        case standard = "standard"
        case short = "short"
    }
}

// MARK: - ArtistPurplePlayParams
struct ArtistPurplePlayParams: Codable {
    var id: String?
    var kind: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case kind = "kind"
    }
}

// MARK: - ArtistGenres
struct ArtistGenres: Codable {
    var href: String?
    var data: [ArtistGenresDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - ArtistGenresDatum
struct ArtistGenresDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistTentacledAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistTentacledAttributes
struct ArtistTentacledAttributes: Codable {
    var parentId: String?
    var parentName: String?
    var name: String?
    var url: String?

    enum CodingKeys: String, CodingKey {
        case parentId = "parentId"
        case parentName = "parentName"
        case name = "name"
        case url = "url"
    }
}

// MARK: - ArtistMusicVideos
struct ArtistMusicVideos: Codable {
    var href: String?
    var data: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - ArtistRelationshipsPlaylists
struct ArtistRelationshipsPlaylists: Codable {
    var href: String?
    var data: [ArtistPlaylistsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - ArtistPlaylistsDatum
struct ArtistPlaylistsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistStickyAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistStickyAttributes
struct ArtistStickyAttributes: Codable {
    var artwork: ArtistArtwork?
    var isChart: Bool?
    var url: String?
    var trackCount: Int?
    var lastModifiedDate: String?
    var name: String?
    var playlistType: String?
    var editorialArtwork: ArtistTentacledEditorialArtwork?
    var curatorName: String?
    var playParams: ArtistPurplePlayParams?
    var attributesDescription: ArtistEditorialNotes?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case isChart = "isChart"
        case url = "url"
        case trackCount = "trackCount"
        case lastModifiedDate = "lastModifiedDate"
        case name = "name"
        case playlistType = "playlistType"
        case editorialArtwork = "editorialArtwork"
        case curatorName = "curatorName"
        case playParams = "playParams"
        case attributesDescription = "description"
    }
}

// MARK: - ArtistTentacledEditorialArtwork
struct ArtistTentacledEditorialArtwork: Codable {
    var subscriptionHero: ArtistArtwork?
    var subscriptionCover: ArtistArtwork?

    enum CodingKeys: String, CodingKey {
        case subscriptionHero = "subscriptionHero"
        case subscriptionCover = "subscriptionCover"
    }
}

// MARK: - ArtistSongs
struct ArtistSongs: Codable {
    var href: String?
    var next: String?
    var data: [ArtistSongsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case next = "next"
        case data = "data"
    }
}

// MARK: - ArtistSongsDatum
struct ArtistSongsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistIndigoAttributes?
    var relationships: ArtistFluffyRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - ArtistIndigoAttributes
struct ArtistIndigoAttributes: Codable {
    var previews: [ArtistPreview]?
    var artwork: ArtistArtwork?
    var artistName: String?
    var url: String?
    var discNumber: Int?
    var genreNames: [String]?
    var hasTimeSyncedLyrics: Bool?
    var durationInMillis: Int?
    var releaseDate: String?
    var name: String?
    var editorialArtwork: ArtistPurpleEditorialArtwork?
    var isrc: String?
    var hasLyrics: Bool?
    var albumName: String?
    var playParams: ArtistPurplePlayParams?
    var trackNumber: Int?
    var composerName: String?
    var contentRating: String?

    enum CodingKeys: String, CodingKey {
        case previews = "previews"
        case artwork = "artwork"
        case artistName = "artistName"
        case url = "url"
        case discNumber = "discNumber"
        case genreNames = "genreNames"
        case hasTimeSyncedLyrics = "hasTimeSyncedLyrics"
        case durationInMillis = "durationInMillis"
        case releaseDate = "releaseDate"
        case name = "name"
        case editorialArtwork = "editorialArtwork"
        case isrc = "isrc"
        case hasLyrics = "hasLyrics"
        case albumName = "albumName"
        case playParams = "playParams"
        case trackNumber = "trackNumber"
        case composerName = "composerName"
        case contentRating = "contentRating"
    }
}

// MARK: - ArtistPreview
struct ArtistPreview: Codable {
    var url: String?

    enum CodingKeys: String, CodingKey {
        case url = "url"
    }
}

// MARK: - ArtistFluffyRelationships
struct ArtistFluffyRelationships: Codable {
    var station: ArtistPurpleStation?

    enum CodingKeys: String, CodingKey {
        case station = "station"
    }
}

// MARK: - ArtistPurpleStation
struct ArtistPurpleStation: Codable {
    var href: String?
    var data: [ArtistPurpleDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - ArtistPurpleDatum
struct ArtistPurpleDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistIndecentAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistIndecentAttributes
struct ArtistIndecentAttributes: Codable {
    var artwork: ArtistArtwork?
    var playParams: ArtistFluffyPlayParams?
    var url: String?
    var isLive: Bool?
    var requiresSubscription: Bool?
    var mediaKind: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case playParams = "playParams"
        case url = "url"
        case isLive = "isLive"
        case requiresSubscription = "requiresSubscription"
        case mediaKind = "mediaKind"
        case name = "name"
    }
}

// MARK: - ArtistFluffyPlayParams
struct ArtistFluffyPlayParams: Codable {
    var id: String?
    var kind: String?
    var format: String?
    var stationHash: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case kind = "kind"
        case format = "format"
        case stationHash = "stationHash"
    }
}

// MARK: - ArtistFluffyStation
struct ArtistFluffyStation: Codable {
    var href: String?
    var data: [ArtistFluffyDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - ArtistFluffyDatum
struct ArtistFluffyDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistHilariousAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistHilariousAttributes
struct ArtistHilariousAttributes: Codable {
    var artwork: ArtistArtwork?
    var playParams: ArtistFluffyPlayParams?
    var url: String?
    var isLive: Bool?
    var requiresSubscription: Bool?
    var mediaKind: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case playParams = "playParams"
        case url = "url"
        case isLive = "isLive"
        case requiresSubscription = "requiresSubscription"
        case mediaKind = "mediaKind"
        case name = "name"
    }
}

// MARK: - ArtistDatumViews
struct ArtistDatumViews: Codable {
    var liveAlbums: ArtistLiveAlbums?
    var featuredRelease: ArtistAppearsOnAlbums?
    var featuredAlbums: ArtistAppearsOnAlbums?
    var latestRelease: ArtistLatestRelease?
    var topSongs: ArtistTopSongs?
    var similarArtists: ArtistSimilarArtists?
    var appearsOnAlbums: ArtistAppearsOnAlbums?
    var topMusicVideos: ArtistAppearsOnAlbums?
    var playlists: ArtistViewsPlaylists?
    var compilationAlbums: ArtistCompilationAlbums?
    var singles: ArtistSingles?
    var fullAlbums: ArtistFullAlbums?

    enum CodingKeys: String, CodingKey {
        case liveAlbums = "live-albums"
        case featuredRelease = "featured-release"
        case featuredAlbums = "featured-albums"
        case latestRelease = "latest-release"
        case topSongs = "top-songs"
        case similarArtists = "similar-artists"
        case appearsOnAlbums = "appears-on-albums"
        case topMusicVideos = "top-music-videos"
        case playlists = "playlists"
        case compilationAlbums = "compilation-albums"
        case singles = "singles"
        case fullAlbums = "full-albums"
    }
}

// MARK: - ArtistAppearsOnAlbums
struct ArtistAppearsOnAlbums: Codable {
    var href: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [JSONAny]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistAppearsOnAlbumsAttributes
struct ArtistAppearsOnAlbumsAttributes: Codable {
    var title: String?

    enum CodingKeys: String, CodingKey {
        case title = "title"
    }
}

// MARK: - ArtistCompilationAlbums
struct ArtistCompilationAlbums: Codable {
    var href: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistCompilationAlbumsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistCompilationAlbumsDatum
struct ArtistCompilationAlbumsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistAmbitiousAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistAmbitiousAttributes
struct ArtistAmbitiousAttributes: Codable {
    var artwork: ArtistArtwork?
    var artistName: String?
    var isSingle: Bool?
    var url: String?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var name: String?
    var recordLabel: String?
    var editorialArtwork: ArtistPurpleEditorialArtwork?
    var copyright: String?
    var playParams: ArtistPurplePlayParams?
    var editorialNotes: ArtistPurpleEditorialNotes?
    var isCompilation: Bool?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case url = "url"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case name = "name"
        case recordLabel = "recordLabel"
        case editorialArtwork = "editorialArtwork"
        case copyright = "copyright"
        case playParams = "playParams"
        case editorialNotes = "editorialNotes"
        case isCompilation = "isCompilation"
    }
}

// MARK: - ArtistPurpleEditorialNotes
struct ArtistPurpleEditorialNotes: Codable {
    var standard: String?

    enum CodingKeys: String, CodingKey {
        case standard = "standard"
    }
}

// MARK: - ArtistFullAlbums
struct ArtistFullAlbums: Codable {
    var href: String?
    var next: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistFullAlbumsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case next = "next"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistLatestRelease
struct ArtistLatestRelease: Codable {
    var href: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistLatestReleaseDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistLatestReleaseDatum
struct ArtistLatestReleaseDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistCunningAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistCunningAttributes
struct ArtistCunningAttributes: Codable {
    var artwork: ArtistArtwork?
    var artistName: String?
    var isSingle: Bool?
    var url: String?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var name: String?
    var recordLabel: String?
    var editorialArtwork: ArtistPurpleEditorialArtwork?
    var copyright: String?
    var playParams: ArtistPurplePlayParams?
    var isCompilation: Bool?
    var contentRating: String?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case url = "url"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case name = "name"
        case recordLabel = "recordLabel"
        case editorialArtwork = "editorialArtwork"
        case copyright = "copyright"
        case playParams = "playParams"
        case isCompilation = "isCompilation"
        case contentRating = "contentRating"
    }
}

// MARK: - ArtistLiveAlbums
struct ArtistLiveAlbums: Codable {
    var href: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistLiveAlbumsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistLiveAlbumsDatum
struct ArtistLiveAlbumsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistMagentaAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistMagentaAttributes
struct ArtistMagentaAttributes: Codable {
    var artwork: ArtistArtwork?
    var artistName: String?
    var isSingle: Bool?
    var url: String?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var name: String?
    var recordLabel: String?
    var editorialArtwork: ArtistPurpleEditorialArtwork?
    var copyright: String?
    var playParams: ArtistPurplePlayParams?
    var isCompilation: Bool?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case url = "url"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case name = "name"
        case recordLabel = "recordLabel"
        case editorialArtwork = "editorialArtwork"
        case copyright = "copyright"
        case playParams = "playParams"
        case isCompilation = "isCompilation"
    }
}

// MARK: - ArtistViewsPlaylists
struct ArtistViewsPlaylists: Codable {
    var href: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistPlaylistsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistSimilarArtists
struct ArtistSimilarArtists: Codable {
    var href: String?
    var next: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistSimilarArtistsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case next = "next"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistSimilarArtistsDatum
struct ArtistSimilarArtistsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistFriskyAttributes?
    var relationships: ArtistTentacledRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - ArtistFriskyAttributes
struct ArtistFriskyAttributes: Codable {
    var origin: String?
    var editorialArtwork: ArtistStickyEditorialArtwork?
    var url: String?
    var artwork: ArtistArtwork?
    var genreNames: [String]?
    var bornOrFormed: String?
    var artistBio: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case origin = "origin"
        case editorialArtwork = "editorialArtwork"
        case url = "url"
        case artwork = "artwork"
        case genreNames = "genreNames"
        case bornOrFormed = "bornOrFormed"
        case artistBio = "artistBio"
        case name = "name"
    }
}

// MARK: - ArtistStickyEditorialArtwork
struct ArtistStickyEditorialArtwork: Codable {
    var originalFlowcaseBrick: ArtistArtwork?

    enum CodingKeys: String, CodingKey {
        case originalFlowcaseBrick = "originalFlowcaseBrick"
    }
}

// MARK: - ArtistTentacledRelationships
struct ArtistTentacledRelationships: Codable {
    var albums: ArtistFluffyAlbums?
    var station: ArtistTentacledStation?

    enum CodingKeys: String, CodingKey {
        case albums = "albums"
        case station = "station"
    }
}

// MARK: - ArtistFluffyAlbums
struct ArtistFluffyAlbums: Codable {
    var href: String?
    var data: [ArtistTentacledDatum]?
    var next: String?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
        case next = "next"
    }
}

// MARK: - ArtistTentacledDatum
struct ArtistTentacledDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistMischievousAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistMischievousAttributes
struct ArtistMischievousAttributes: Codable {
    var artwork: ArtistArtwork?
    var artistName: String?
    var isSingle: Bool?
    var url: String?
    var isComplete: Bool?
    var genreNames: [String]?
    var trackCount: Int?
    var isMasteredForItunes: Bool?
    var releaseDate: String?
    var name: String?
    var recordLabel: String?
    var editorialArtwork: ArtistIndigoEditorialArtwork?
    var copyright: String?
    var playParams: ArtistPurplePlayParams?
    var isCompilation: Bool?
    var contentRating: String?
    var editorialNotes: ArtistEditorialNotes?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case artistName = "artistName"
        case isSingle = "isSingle"
        case url = "url"
        case isComplete = "isComplete"
        case genreNames = "genreNames"
        case trackCount = "trackCount"
        case isMasteredForItunes = "isMasteredForItunes"
        case releaseDate = "releaseDate"
        case name = "name"
        case recordLabel = "recordLabel"
        case editorialArtwork = "editorialArtwork"
        case copyright = "copyright"
        case playParams = "playParams"
        case isCompilation = "isCompilation"
        case contentRating = "contentRating"
        case editorialNotes = "editorialNotes"
    }
}

// MARK: - ArtistIndigoEditorialArtwork
struct ArtistIndigoEditorialArtwork: Codable {
    var originalFlowcaseBrick: ArtistArtwork?
    var storeFlowcase: ArtistArtwork?
    var subscriptionHero: ArtistArtwork?

    enum CodingKeys: String, CodingKey {
        case originalFlowcaseBrick = "originalFlowcaseBrick"
        case storeFlowcase = "storeFlowcase"
        case subscriptionHero = "subscriptionHero"
    }
}

// MARK: - ArtistTentacledStation
struct ArtistTentacledStation: Codable {
    var href: String?
    var data: [ArtistStickyDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - ArtistStickyDatum
struct ArtistStickyDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistBraggadociousAttributes?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistBraggadociousAttributes
struct ArtistBraggadociousAttributes: Codable {
    var artwork: ArtistArtwork?
    var playParams: ArtistFluffyPlayParams?
    var url: String?
    var isLive: Bool?
    var requiresSubscription: Bool?
    var mediaKind: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case playParams = "playParams"
        case url = "url"
        case isLive = "isLive"
        case requiresSubscription = "requiresSubscription"
        case mediaKind = "mediaKind"
        case name = "name"
    }
}

// MARK: - ArtistSingles
struct ArtistSingles: Codable {
    var href: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistLatestReleaseDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistTopSongs
struct ArtistTopSongs: Codable {
    var href: String?
    var next: String?
    var attributes: ArtistAppearsOnAlbumsAttributes?
    var data: [ArtistTopSongsDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case next = "next"
        case attributes = "attributes"
        case data = "data"
    }
}

// MARK: - ArtistTopSongsDatum
struct ArtistTopSongsDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistAttributes1?
    var relationships: ArtistStickyRelationships?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
        case relationships = "relationships"
    }
}

// MARK: - ArtistAttributes1
struct ArtistAttributes1: Codable {
    var previews: [ArtistPreview]?
    var artwork: ArtistArtwork?
    var artistName: String?
    var url: String?
    var discNumber: Int?
    var genreNames: [String]?
    var hasTimeSyncedLyrics: Bool?
    var durationInMillis: Int?
    var releaseDate: String?
    var name: String?
    var editorialArtwork: ArtistPurpleEditorialArtwork?
    var isrc: String?
    var hasLyrics: Bool?
    var albumName: String?
    var playParams: ArtistPurplePlayParams?
    var trackNumber: Int?
    var composerName: String?

    enum CodingKeys: String, CodingKey {
        case previews = "previews"
        case artwork = "artwork"
        case artistName = "artistName"
        case url = "url"
        case discNumber = "discNumber"
        case genreNames = "genreNames"
        case hasTimeSyncedLyrics = "hasTimeSyncedLyrics"
        case durationInMillis = "durationInMillis"
        case releaseDate = "releaseDate"
        case name = "name"
        case editorialArtwork = "editorialArtwork"
        case isrc = "isrc"
        case hasLyrics = "hasLyrics"
        case albumName = "albumName"
        case playParams = "playParams"
        case trackNumber = "trackNumber"
        case composerName = "composerName"
    }
}

// MARK: - ArtistStickyRelationships
struct ArtistStickyRelationships: Codable {
    var station: ArtistStickyStation?

    enum CodingKeys: String, CodingKey {
        case station = "station"
    }
}

// MARK: - ArtistStickyStation
struct ArtistStickyStation: Codable {
    var href: String?
    var data: [ArtistIndigoDatum]?

    enum CodingKeys: String, CodingKey {
        case href = "href"
        case data = "data"
    }
}

// MARK: - ArtistIndigoDatum
struct ArtistIndigoDatum: Codable {
    var id: String?
    var type: String?
    var href: String?
    var attributes: ArtistAttributes2?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case type = "type"
        case href = "href"
        case attributes = "attributes"
    }
}

// MARK: - ArtistAttributes2
struct ArtistAttributes2: Codable {
    var artwork: ArtistArtwork?
    var playParams: ArtistFluffyPlayParams?
    var url: String?
    var isLive: Bool?
    var requiresSubscription: Bool?
    var mediaKind: String?
    var name: String?

    enum CodingKeys: String, CodingKey {
        case artwork = "artwork"
        case playParams = "playParams"
        case url = "url"
        case isLive = "isLive"
        case requiresSubscription = "requiresSubscription"
        case mediaKind = "mediaKind"
        case name = "name"
    }
}
