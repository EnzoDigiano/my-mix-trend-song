//
//  MusicItem.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 12/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import RealmSwift

class MusicItem: Object {

    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var name: String = ""
    @objc dynamic var descriptions: String = ""
    @objc dynamic var cover: String = ""
    @objc dynamic var genre: String = ""

    override static func primaryKey() -> String? {
        return "id"
    }
    
    /// Convert a `MusicItem` object into `favorite`
    /// - Returns: `Favorite` object
    func toFavorite() -> Favorite {
        let result = Favorite()
        
        result.id = self.id
        result.tile = self.name
        result.descriptions = self.descriptions
        result.cover = self.cover
        result.genre = self.genre
        
        return result
    }
}
