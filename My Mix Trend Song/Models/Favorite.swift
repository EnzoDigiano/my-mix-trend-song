//
//  Favorite.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

import RealmSwift

class Favorite: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var tile: String = ""
    @objc dynamic var descriptions: String = ""
    @objc dynamic var cover: String = ""
    @objc dynamic var genre: String = ""
    @objc dynamic var type: Int = 0
    
    override static func primaryKey() -> String? {
        return "id"
    }
}
