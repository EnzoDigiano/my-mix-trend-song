//
//  PlayerModuleRouter.swift
//  PlayerModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation
import UIKit

final class PlayerModuleRouter: RouterInterface {

    weak var presenter: PlayerModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension PlayerModuleRouter: PlayerModuleRouterPresenterInterface {
    
    func showShare(song: Song) {
        let items: [Any] = [URL(string: song.preview)!]
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.viewController?.present(activityController, animated: true)
    }
    
    func showLyric(lyric: String) {
        let lyricViewController = LyricModule().build(lyric: lyric)
        let navigationController = UINavigationController(rootViewController: lyricViewController)
        viewController?.present(navigationController, animated: true)
    }
}
