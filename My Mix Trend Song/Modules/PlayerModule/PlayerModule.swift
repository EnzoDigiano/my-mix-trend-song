//
//  PlayerModuleModule.swift
//  PlayerModule
//
//  Created by Enzo Digiano on 1/8/20.
//
import Foundation
import UIKit

// MARK: - module builder
final class PlayerModule: ModuleInterface {

    typealias View = PlayerViewController
    typealias Presenter = PlayerModulePresenter
    typealias Router = PlayerModuleRouter
    typealias Interactor = PlayerModuleInteractor

    func build(song: Song) -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()
        
        view.song = song
        
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}

#if DEBUG
import SwiftUI

@available(iOS 13, *)
struct PlayerModulePreview: PreviewProvider {
    
    static var devices = ["iPhone 11 Pro Max"]
    
    static var previews: some View {
        ForEach(devices, id: \.self) { deviceName in
            PlayerModule().build(song: Song()).toPreview()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
#endif
