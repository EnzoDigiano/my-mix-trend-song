//
//  PlayerModuleProtocols.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - router
protocol PlayerModuleRouterPresenterInterface: RouterPresenterInterface {
    func showLyric(lyric: String)
    func showShare(song: Song)
}

// MARK: - interactor
protocol PlayerModuleInteractorPresenterInterface: InteractorPresenterInterface {
    func makeGetLyric(artist: String, song: String)
    func makeSetFavorite(song: Song)
    func makeGetFavoriteState(song: Song)
}

// MARK: - presenter
protocol PlayerModulePresenterRouterInterface: PresenterRouterInterface {

}

protocol PlayerModulePresenterInteractorInterface: PresenterInteractorInterface {
    func didGetLyric(result: Result<String, Error>)
    func didSetFavorite(state: Bool)
}

protocol PlayerModulePresenterViewInterface: PresenterViewInterface {
    func startGetLyric(song: Song)
    func startShare(song: Song)
    func startShowLyric(lyric: String)
    func startFavoriteToggle(song: Song)
    func startGetFavoriteState(song: Song)
}

// MARK: - view
protocol PlayerModuleViewPresenterInterface: ViewPresenterInterface {
    var song:Song? { get set }
    var lyric:String { get set }
    func fetchData()
    func configurePlayButton()
    func updateFavorite(with state: Bool)
}
