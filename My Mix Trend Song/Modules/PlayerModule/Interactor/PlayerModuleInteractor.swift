//
//  PlayerModuleInteractor.swift
//  PlayerModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation
import Realm
import PromiseKit

final class PlayerModuleInteractor: InteractorInterface {
    weak var presenter: PlayerModulePresenterInteractorInterface!
    fileprivate let favoriteService = FavoriteService()
}

// MARK: - Presenter
extension PlayerModuleInteractor: PlayerModuleInteractorPresenterInterface {
    
    func makeGetFavoriteState(song: Song) {
        presenter.didSetFavorite(state: favoriteService.isFavorite(with: song.id))
    }
    
    func makeSetFavorite(song: Song) {
        if favoriteService.isFavorite(with: song.id){
            favoriteService.delete(song.id)
            presenter.didSetFavorite(state: false)
        } else {
            let favorite = song.toFavorite()
            favoriteService.store(favorite)
            presenter.didSetFavorite(state: true)
        }
    }
    
    func makeGetLyric(artist: String, song: String) {
        let service = MusixmatchService()
        firstly {
            service.GetLyric(artist: artist, track: song)
        }.done { response in
            guard let lyric = response.message?.body?.lyrics?.lyricsBody else {
                print("There be an error parsing lyric data")
                self.presenter?.didGetLyric(result:.failure(ResponseError.getData))
                return
            }
            self.presenter?.didGetLyric(result:.success(lyric))
        }.catch { error in
            print("There be an error getting lyric: \(error.localizedDescription)")
            self.presenter?.didGetLyric(result:.failure(error))
        }
    }
}
