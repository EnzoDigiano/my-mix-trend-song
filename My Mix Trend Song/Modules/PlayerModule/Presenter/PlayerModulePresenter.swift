//
//  PlayerModulePresenter.swift
//  PlayerModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation

final class PlayerModulePresenter: PresenterInterface {

    var router: PlayerModuleRouterPresenterInterface!
    var interactor: PlayerModuleInteractorPresenterInterface!
    weak var view: PlayerModuleViewPresenterInterface!

}

// MARK: - Router
extension PlayerModulePresenter: PlayerModulePresenterRouterInterface {

}

// MARK: - Interactor
extension PlayerModulePresenter: PlayerModulePresenterInteractorInterface {
    
    func didSetFavorite(state: Bool) {
        view.updateFavorite(with: state)
    }

    func didGetLyric(result: Result<String, Error>) {
        switch result {
            case .success(let lyric):
                if lyric.count == 0 { return }
                print("Get song lyric: \(lyric)")
                view.lyric = lyric
                break
            case .failure(let error):
                print("There be an error fetching song lyric: \(error.localizedDescription)")
                break
        }
    }
}

// MARK: - View
extension PlayerModulePresenter: PlayerModulePresenterViewInterface {
    
    func startGetFavoriteState(song: Song) {
        interactor.makeGetFavoriteState(song: song)
    }
    
    func startFavoriteToggle(song: Song) {
        interactor.makeSetFavorite(song: song)
    }

    func startShare(song: Song) {
        router.showShare(song: song)
    }
    
    func startShowLyric(lyric: String) {
        router.showLyric(lyric: lyric)
    }
    
    func startGetLyric(song: Song) {
        interactor.makeGetLyric(artist: song.artistName, song: song.songName)
    }
}
