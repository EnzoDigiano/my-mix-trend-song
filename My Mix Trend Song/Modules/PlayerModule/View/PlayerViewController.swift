//
//  PlayerViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit
import Kingfisher
import AVFoundation

class PlayerViewController: UIViewController, ViewInterface {
    
    var presenter: PlayerModulePresenterViewInterface!
    var song: Song? = nil
    var player = AVPlayer()
    var isPlaying: Bool = false
    var lyric: String = "" {
        didSet {
            UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                self.showLyricButton.isHidden = false
            })
        }
    }
    
    @IBOutlet weak var cover: UIImageView!
    @IBOutlet weak var songLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel!
    @IBOutlet weak var genereLabel: UILabel!
    @IBOutlet weak var favoriteButton: UIButton!
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var showLyricButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.fetchData()
        // Do any additional setup after loading the view.
        
        configurePlayButton()
        preferredContentSize = cover.image?.size as! CGSize
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let backImage = UIImage(systemName: "arrow.left.circle.fill")

        navigationController?.navigationBar.backIndicatorImage = backImage
        navigationController?.navigationBar.backIndicatorTransitionMaskImage = backImage
        navigationController?.navigationBar.tintColor = .systemRed
        
        let backButton = UIBarButtonItem()
        backButton.title = ""
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = backButton
        
        guard let song = self.song else { return }
        presenter.startGetFavoriteState(song: song)
    }

    
    @IBAction func favoriteButtonDidTap(_ sender: Any) {
        guard let song = self.song else { return }
        presenter.startFavoriteToggle(song: song)
    }
    
    @IBAction func playButtonDidTap(_ sender: Any) {
        let playerItem = AVPlayerItem(url: URL(string: song?.preview ?? "" )!)
        player = AVPlayer(playerItem: playerItem)
        NotificationCenter.default.addObserver(self, selector:#selector(self.playerDidFinishPlaying(note:)),name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        if self.isPlaying {
            self.player.pause()
            self.isPlaying = false
        } else {
            self.player.play()
            self.isPlaying = true
        }
        self.configurePlayButton()
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
       isPlaying = false
       self.configurePlayButton()
    }
    
    @IBAction func shareButtonDidTap(_ sender: Any) {
        guard let song = self.song else { return }
        presenter.startShare(song: song)
    }
    
    @IBAction func showLyricButtonDidTap(_ sender: Any) {
        presenter.startShowLyric(lyric: self.lyric)
    }
}

// MARK: - Presenter
extension PlayerViewController: PlayerModuleViewPresenterInterface {
    
    func updateFavorite(with state: Bool) {
        if state {
            let image = UIImage(systemName: "suit.heart.fill")
            favoriteButton.setImage(image, for: .normal)
        } else {
            let image = UIImage(systemName: "suit.heart")
            favoriteButton.setImage(image, for: .normal)
        }
    }
    
    
    internal func fetchData() {
        guard let song = song else { return }
        let url = song.cover
        let urlImage = url.replacingOccurrences(of: "{w}x{h}", with: "800x800")
        if let coverURL = URL(string: urlImage) {
            cover.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
            let topShadow = EdgeShadowLayer(forView: cover, edge: .Top)
            cover.layer.addSublayer(topShadow)
            cover.kf.setImage(with: .network(coverURL), options: [.transition(.fade(0.2))]) { result in
                switch result {
                    case .success( _):
                        print("Sucessfully get image")
                    case .failure( _):
                        print("Fail to get image")
                }
            }
        } else {
           print("Fail to make cover URL")
        }
        
        songLabel.text = song.songName
        artistLabel.text = "\(song.artistName) - \(song.albumName)"
        genereLabel.text = song.genre
        
        presenter.startGetLyric(song: song)
    }
    
    internal func configurePlayButton() {
        let boldFont = UIFont.boldSystemFont(ofSize: 45)
        let configuration = UIImage.SymbolConfiguration(font: boldFont)
        playButton.tintColor = UIColor.white
        playButton.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        playButton.layer.cornerRadius = 0.5 * playButton.bounds.size.width
        playButton.clipsToBounds = true
        playButton.setImage( UIImage(systemName: "play.circle", withConfiguration: configuration), for: .normal)
        if isPlaying {
            playButton.setImage( UIImage(systemName: "stop.circle", withConfiguration: configuration), for: .normal)
        }
    }
}
