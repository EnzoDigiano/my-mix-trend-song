//
//  MainTabModuleModule.swift
//  MainTabModule
//
//  Created by Enzo Digiano on 1/8/20.
//
import Foundation
import UIKit

// MARK: - module builder
final class MainTabModule: ModuleInterface {

    typealias View = MainTabBarController
    typealias Presenter = MainTabModulePresenter
    typealias Router = MainTabModuleRouter
    typealias Interactor = MainTabModuleInteractor

    func build() -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        UITabBar.appearance().tintColor = UIColor.red
        
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}

#if DEBUG
import SwiftUI

@available(iOS 13, *)
struct MainTabModulePreview: PreviewProvider {
    
    static var devices = ["iPhone 11 Pro Max"]
    
    static var previews: some View {
        ForEach(devices, id: \.self) { deviceName in
            MainTabModule().build().toPreview()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
#endif
