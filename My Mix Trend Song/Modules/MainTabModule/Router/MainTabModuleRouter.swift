//
//  MainTabModuleRouter.swift
//  MainTabModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation
import UIKit

final class MainTabModuleRouter: RouterInterface {
    weak var presenter: MainTabModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension MainTabModuleRouter: MainTabModuleRouterPresenterInterface {
    
    func getTabs() {
        let favorites = getFavorites()
        let songs = getSongs()
        let searh = getSearch()
        presenter.didGetTabs(tabs:(favorites: favorites, songs: songs, searh: searh))
    }
}

// MARK: - Private functions
extension MainTabModuleRouter {
    
    fileprivate func getFavorites() -> UIViewController{
        let navigationController = UINavigationController()
        let viewController = FavoriteModule().build()
        viewController.tabBarItem = UITabBarItem(title: "Favorite",
                                          image: UIImage(systemName: "suit.heart"),
                                          selectedImage: UIImage(systemName: "suit.heart"))
        navigationController.setViewControllers([viewController], animated: false)
        return navigationController
    }
    
    fileprivate func getSongs() -> UIViewController{
        let navigationController = UINavigationController()
        let viewController = SongsModule().build()
        viewController.tabBarItem = UITabBarItem(title: "Song",
                                                 image: UIImage(systemName:  "music.note"),
                                                 selectedImage: UIImage(systemName:  "music.note"))
        navigationController.setViewControllers([viewController], animated: false)
        return navigationController
    }
    
    fileprivate func getSearch() -> UIViewController{
        let navigationController = UINavigationController()
        let viewController = SearchSongModule().build()
        viewController.tabBarItem = UITabBarItem(title: "Search",
                                                 image: UIImage(systemName: "magnifyingglass" ),
                                                 selectedImage: UIImage(systemName: "magnifyingglass"))
        navigationController.setViewControllers([viewController], animated: false)
        return navigationController
    }
}

