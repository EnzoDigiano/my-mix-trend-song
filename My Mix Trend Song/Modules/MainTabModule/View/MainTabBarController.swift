//
//  MainTabBarController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController, ViewInterface {

    var presenter: MainTabModulePresenterViewInterface!
    var tabs: MainTabs? = nil { didSet {
            guard let tabs = tabs else { return }
            viewControllers = [ tabs.favorites,
                                tabs.songs,
                                tabs.searh]
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        presenter.startGetTabs()
        self.selectedIndex = 1
    }
}

// MARK: - Presenter
extension MainTabBarController: MainTabModuleViewPresenterInterface {

}
