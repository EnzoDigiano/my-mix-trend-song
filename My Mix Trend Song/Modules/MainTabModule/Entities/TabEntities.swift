//
//  TabEntities.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import UIKit

typealias MainTabs = (
    favorites: UIViewController,
    songs: UIViewController,
    searh: UIViewController
)
