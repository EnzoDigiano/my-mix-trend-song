//
//  MainTabModuleInteractor.swift
//  MainTabModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation

final class MainTabModuleInteractor: InteractorInterface {

    weak var presenter: MainTabModulePresenterInteractorInterface!
}

extension MainTabModuleInteractor: MainTabModuleInteractorPresenterInterface {

}
