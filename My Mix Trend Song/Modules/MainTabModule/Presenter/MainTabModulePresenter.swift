//
//  MainTabModulePresenter.swift
//  MainTabModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation

final class MainTabModulePresenter: PresenterInterface {

    var router: MainTabModuleRouterPresenterInterface!
    var interactor: MainTabModuleInteractorPresenterInterface!
    weak var view: MainTabModuleViewPresenterInterface!
}

// MARK: - Router
extension MainTabModulePresenter: MainTabModulePresenterRouterInterface {
    
    func didGetTabs(tabs: MainTabs) {
        view.tabs = tabs
    }
}

// MARK: - Interactor
extension MainTabModulePresenter: MainTabModulePresenterInteractorInterface {

}

// MARK: - View
extension MainTabModulePresenter: MainTabModulePresenterViewInterface {
    
    func startGetTabs() {
        router.getTabs()
    }
}
