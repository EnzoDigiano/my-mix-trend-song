//
//  MainTabModuleProtocols.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - router
protocol MainTabModuleRouterPresenterInterface: RouterPresenterInterface {
    func getTabs()
}

// MARK: - interactor
protocol MainTabModuleInteractorPresenterInterface: InteractorPresenterInterface {

}

// MARK: - presenter
protocol MainTabModulePresenterRouterInterface: PresenterRouterInterface {
    func didGetTabs(tabs: MainTabs)
}

protocol MainTabModulePresenterInteractorInterface: PresenterInteractorInterface {

}

protocol MainTabModulePresenterViewInterface: PresenterViewInterface {
    func startGetTabs()
}

// MARK: - view
protocol MainTabModuleViewPresenterInterface: ViewPresenterInterface {
    var tabs: MainTabs? { get set }
}
