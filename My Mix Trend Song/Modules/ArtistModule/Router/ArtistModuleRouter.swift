//
//  ArtistModuleRouter.swift
//  ArtistModule
//
//  Created by Enzo Digiano on 7/8/20.
//

import Foundation
import UIKit

final class ArtistModuleRouter: RouterInterface {

    weak var presenter: ArtistModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension ArtistModuleRouter: ArtistModuleRouterPresenterInterface {

    func dismiss(){
        self.viewController?.dismiss(animated: true, completion: nil)
    }
}
