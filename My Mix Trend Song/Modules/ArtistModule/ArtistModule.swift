//
//  ArtistModuleModule.swift
//  ArtistModule
//
//  Created by Enzo Digiano on 7/8/20.
//
import Foundation
import UIKit

// MARK: - module builder
final class ArtistModule: ModuleInterface {

    typealias View = ArtistModuleViewController
    typealias Presenter = ArtistModulePresenter
    typealias Router = ArtistModuleRouter
    typealias Interactor = ArtistModuleInteractor

    func build(with id: String) -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        view.id = id
        
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}

#if DEBUG
import SwiftUI

@available(iOS 13, *)
struct ArtistModulePreview: PreviewProvider {
    
    static var devices = ["iPhone 11 Pro Max"]
    
    static var previews: some View {
        ForEach(devices, id: \.self) { deviceName in
            ArtistModule().build(with: "2819846").toPreview()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
#endif
