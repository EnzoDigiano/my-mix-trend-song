//
//  ArtistModuleProtocols.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 07/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import UIKit

// MARK: - router
protocol ArtistModuleRouterPresenterInterface: RouterPresenterInterface {
    func dismiss()
}

// MARK: - interactor
protocol ArtistModuleInteractorPresenterInterface: InteractorPresenterInterface {
    func getArtist(with id: String)
}

// MARK: - presenter
protocol ArtistModulePresenterRouterInterface: PresenterRouterInterface {
    
}

protocol ArtistModulePresenterInteractorInterface: PresenterInteractorInterface {
    func didGetArtist(result: Result<ArtistResponseDatum, Error>)
}

protocol ArtistModulePresenterViewInterface: PresenterViewInterface {
    func startDismiss()
    func startGetArtist(with id: String)
}

// MARK: - view
protocol ArtistModuleViewPresenterInterface: ViewPresenterInterface {
    var id: String? { get set }
    var artist: Artist? { get set }
}
