//
//  ArtistModuleInteractor.swift
//  ArtistModule
//
//  Created by Enzo Digiano on 7/8/20.
//

import Foundation
import PromiseKit

final class ArtistModuleInteractor: InteractorInterface {

    weak var presenter: ArtistModulePresenterInteractorInterface!
}

// MARK: - Presenter
extension ArtistModuleInteractor: ArtistModuleInteractorPresenterInterface {
    
    func getArtist(with id: String) {
        let service = AppleMusicService()
        firstly {
            service.getArtist(with: id)
        }.done { response in
            var artist: [ArtistResponseDatum] = []
            guard let data = response.data else {
                print("There be an error parsing artist data")
                self.presenter?.didGetArtist(result:.failure(ResponseError.getData))
                return
            }
            for item in data {
                artist.append(item)
            }
            self.presenter?.didGetArtist(result:.success(artist.first!))
        }.catch { error in
            print("There be an error getting artist: \(error.localizedDescription)")
            self.presenter?.didGetArtist(result:.failure(error))
        }
    }
}
