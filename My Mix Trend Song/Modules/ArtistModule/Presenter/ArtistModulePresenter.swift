//
//  ArtistModulePresenter.swift
//  ArtistModule
//
//  Created by Enzo Digiano on 7/8/20.
//

import Foundation

final class ArtistModulePresenter: PresenterInterface {

    var router: ArtistModuleRouterPresenterInterface!
    var interactor: ArtistModuleInteractorPresenterInterface!
    weak var view: ArtistModuleViewPresenterInterface!
}

// MARK: - Router
extension ArtistModulePresenter: ArtistModulePresenterRouterInterface {
    
}

//MARK: Interactor
extension ArtistModulePresenter: ArtistModulePresenterInteractorInterface {
    
    func didGetArtist(result: Result<ArtistResponseDatum, Error>) {
        switch result {
            case .success(let result):
                view.artist = result.toArtist()
                break
            case .failure(let error):
                print("Couldn't get artist: \(error)")
                break
        }
    }
}

// MARK: View
extension ArtistModulePresenter: ArtistModulePresenterViewInterface {
    
    func startGetArtist(with id: String) {
        interactor.getArtist(with: id)
    }

    func startDismiss(){
        router.dismiss()
    }

}
