//
//  ArtistModuleViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 07/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class ArtistModuleViewController: UIViewController , ViewInterface {

    var presenter: ArtistModulePresenterViewInterface!
    var artist: Artist? = nil { didSet {
            print("Set Artist")
            configure()
        }
    }
    var id: String? = nil
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var artistBioLabel: UILabel!
    @IBOutlet weak var artistNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let id = id else { return }
        self.showSpinner()
        presenter.startGetArtist(with: id)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // Make the navigation bar background clear
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.tintColor = .systemRed
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "xmark.square.fill"), style: .plain, target: self, action: #selector(close))
    }
    
    @objc internal func close(){
        presenter.startDismiss()
    }
    
    internal func configure(){
        guard let artist = artist else { return }
        artistBioLabel.text = artist.artistBio
        artistNameLabel.text = artist.name
        containerView.cornerRadius = 25
        containerView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        let url = artist.cover
        let urlImage = url.replacingOccurrences(of: "{w}x{h}", with: "800x800")
        if let coverURL = URL(string: urlImage) {
            coverImage.kf.setImage(with: .network(coverURL), options: [.transition(.fade(0.2))]) { result in
                self.stopSpinner()
                switch result {
                    case .success( _):
                        print("Sucessfully get image")
                    case .failure( _):
                        print("Fail to get image")
                }
            }
        } else {
           print("Fail to make cover URL")
        }
    }
}

// MARK: - Presenter
extension ArtistModuleViewController: ArtistModuleViewPresenterInterface {

}
