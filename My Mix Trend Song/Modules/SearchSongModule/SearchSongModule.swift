//
//  SearchSongModuleModule.swift
//  SearchSongModule
//
//  Created by Enzo Digiano on 5/8/20.
//
import Foundation
import UIKit

// MARK: - module builder
final class SearchSongModule: ModuleInterface {

    typealias View = SearchSongViewController
    typealias Presenter = SearchSongModulePresenter
    typealias Router = SearchSongModuleRouter
    typealias Interactor = SearchSongModuleInteractor

    func build() -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}

#if DEBUG
import SwiftUI

@available(iOS 13, *)
struct SearchSongModulePreview: PreviewProvider {
    
    static var devices = ["iPhone 11 Pro Max"]
    
    static var previews: some View {
        ForEach(devices, id: \.self) { deviceName in
            SearchSongModule().build().toPreview()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
#endif
