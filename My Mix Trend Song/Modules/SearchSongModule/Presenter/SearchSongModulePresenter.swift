//
//  SearchSongModulePresenter.swift
//  SearchSongModule
//
//  Created by Enzo Digiano on 5/8/20.
//

import Foundation
import Kingfisher

final class SearchSongModulePresenter: PresenterInterface {

    var router: SearchSongModuleRouterPresenterInterface!
    var interactor: SearchSongModuleInteractorPresenterInterface!
    weak var view: SearchSongModuleViewPresenterInterface!
}

// MARK: - Router
extension SearchSongModulePresenter: SearchSongModulePresenterRouterInterface {

}

// MARK: - Interactor
extension SearchSongModulePresenter: SearchSongModulePresenterInteractorInterface {
    
    func didSetFavorite(state: Bool) {
        print("Favorite update")
    }
    
    func didGetData(result: Result<SearchSongsResponseResults, Error>) {
        switch result {
            case .success(let data):
                switch view.searchBarScope {
                    case .Artists:
                        var artists: [Artist] = []
                        guard let data = data.artist?.data else {
                            print("There be an error parsing Artist/Albun data")
                            return
                        }
                        for item in data {
                            let artist = item.toArtist()
                            if artist.cover == "" { continue }
                            artists.append(artist)
                        }
                        view.musicItems = artists
                        break
                    case .Albums:
                        var albuns: [Albun] = []
                        guard let data = data.album?.data else {
                            print("There be an error parsing Song/Albun data")
                            return
                        }
                        for item in data { albuns.append(item.toAlbun()) }
                        view.musicItems = albuns
                        break
                    case .Songs:
                        var songs: [Song] = []
                        guard let data = data.song?.data else {
                            print("There be an error parsing Song/Song data")
                            return
                        }
                        for item in data { songs.append(item.toSong()) }
                        view.musicItems = songs
                        break
                }
            case .failure(let error):
                print("Couldn't get songs: \(error)")
                break
        }
    }
    
    func didGetHints(result: Result<[String], Error>) {
        switch result {
            case .success(let results):
                view.hints = results
                break
            case .failure(let error):
                print("Couldn't get hints: \(error)")
                break
        }
    }
}

// MARK: - View
extension SearchSongModulePresenter: SearchSongModulePresenterViewInterface {
    
    func startFavoriteToggle(musicItem: MusicItem, type: SearchBarScope) {
        interactor.makeSetFavorite(musicItem: musicItem, type: type)
    }
    
    func startGetFavoriteState(musicItem: MusicItem) -> Bool {
        return interactor.makeGetFavoriteState(musicItem: musicItem)
    }
    
    func startShowArtist(with id: String) {
        router.showArtist(with: id)
    }
    
    func startShowPlayer(musicItem: MusicItem) {
        router.showPlayer(song: musicItem as! Song)
    }
    
    func startGetData(search: String) {
        interactor.getData(search: search)
    }
    
    func startGetHints(search: String) {
        interactor.getHints(search: search)
    }
}
