//
//  SearchSongModuleRouter.swift
//  SearchSongModule
//
//  Created by Enzo Digiano on 5/8/20.
//

import Foundation
import UIKit

final class SearchSongModuleRouter: RouterInterface {

    weak var presenter: SearchSongModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension SearchSongModuleRouter: SearchSongModuleRouterPresenterInterface {
    
    func showArtist(with id: String) {
        let artistViewController = ArtistModule().build(with: id)
        artistViewController.isModalInPresentation = true
        let navigationController = UINavigationController(rootViewController: artistViewController)
        self.viewController?.present(navigationController, animated: true)
    }
    
    func showPlayer(song: Song) {
        let playerViewController = PlayerModule().build(song: song)
        self.viewController?.navigationController?.pushViewController(playerViewController, animated: true)
    }
}
