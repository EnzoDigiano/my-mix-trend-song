//
//  SearchBarScope.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 07/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

enum SearchBarScope: Int{
    case Artists = 0
    case Albums = 1
    case Songs = 2
}
