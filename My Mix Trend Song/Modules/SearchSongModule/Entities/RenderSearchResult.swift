//
//  RenderSearchResult.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 06/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

enum RenderSearchResult: Int{
    case clear = 0
    case hints = 1
    case data = 2
}
