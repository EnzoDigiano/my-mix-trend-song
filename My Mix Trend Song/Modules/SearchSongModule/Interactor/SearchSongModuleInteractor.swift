//
//  SearchSongModuleInteractor.swift
//  SearchSongModule
//
//  Created by Enzo Digiano on 5/8/20.
//

import Foundation
import PromiseKit

final class SearchSongModuleInteractor: InteractorInterface {

    weak var presenter: SearchSongModulePresenterInteractorInterface!
    fileprivate let favoriteService = FavoriteService()
}

// MARK: - Presenter
extension SearchSongModuleInteractor: SearchSongModuleInteractorPresenterInterface {
    
    func makeSetFavorite(musicItem: MusicItem, type: SearchBarScope) {
        if favoriteService.isFavorite(with: musicItem.id){
            favoriteService.delete(musicItem.id)
            presenter.didSetFavorite(state: false)
        } else {
            let favorite = musicItem.toFavorite()
            favorite.type = type.rawValue
            favoriteService.store(favorite)
            presenter.didSetFavorite(state: true)
        }
    }
    
    func makeGetFavoriteState(musicItem: MusicItem) -> Bool {
        return favoriteService.isFavorite(with: musicItem.id)
    }

    func getHints(search: String) {
        let service = AppleMusicService()
        firstly {
            service.GetHint(search: search)
        }.done { response in
            guard let data = response.results?.terms else {
                print("There be an error parsing data")
                self.presenter?.didGetHints(result:.failure(ResponseError.getData))
                return
            }
            self.presenter?.didGetHints(result:.success(data))
        }.catch { error in
            print("There be an error getting songs: \(error.localizedDescription)")
            self.presenter?.didGetHints(result:.failure(error))
        }
    }
    
    func getData(search: String) {
        let service = AppleMusicService()
        firstly {
            service.SearchSong(search: search)
        }.done { response in
            guard let data = response.results else {
                print("There be an error parsing data")
                self.presenter?.didGetData(result:.failure(ResponseError.getData))
                return
            }
            self.presenter?.didGetData(result:.success(data))
        }.catch { error in
            print("There be an error getting songs: \(error.localizedDescription)")
            self.presenter?.didGetData(result:.failure(error))
        }
    }
}
