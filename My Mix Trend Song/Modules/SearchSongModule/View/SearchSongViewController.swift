//
//  SearchSongViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 05/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class SearchSongViewController: UIViewController, ViewInterface {

    var presenter: SearchSongModulePresenterViewInterface!
    var hints: [String] = [] { didSet { reloadHint() }}
    var musicItems:[MusicItem] = [] { didSet { reloadSong() } }
    var contextMenuSelection: MusicItem? = nil
    var renderMode: RenderSearchResult = .hints
    var searchBarScope : SearchBarScope = .Songs
    let searchController =  UISearchController(searchResultsController: nil)
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
    }
    
    internal func configure(){
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Search song"
        navigationItem.searchController = searchController
        navigationItem.largeTitleDisplayMode = .automatic
        navigationItem.hidesSearchBarWhenScrolling = false
        
        searchController.searchBar.delegate = self
        searchController.searchBar.barStyle = .default
        searchController.searchBar.placeholder = "Search"
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.definesPresentationContext = true
        searchController.searchBar.scopeButtonTitles = ["Artist","Album","Song"]
        searchController.searchBar.selectedScopeButtonIndex = 2
        
        let cancelButtonAttributes = [NSAttributedString.Key.foregroundColor: UIColor.systemRed]
        UIBarButtonItem.appearance().setTitleTextAttributes(cancelButtonAttributes , for: .normal)
        
        tableView.register(UINib(nibName: "HintsTableViewCell", bundle: nil), forCellReuseIdentifier: "HintCell")
        tableView.register(UINib(nibName: "SongTableViewCell", bundle: nil), forCellReuseIdentifier: "SongCell")
        tableView.tableFooterView = UIView()
        
        self.shouldDismissKeyboard()
    }
    
    internal func reloadHint(){
        if hints.count > 0 {
            renderMode = .hints
            musicItems = []
            tableView.reloadData()
        }
    }
    
    internal func reloadSong(){
        if musicItems.count > 0 {
            renderMode = .data
            hints = []
            tableView.reloadData()
            self.stopSpinner()
        }
    }
}

// MARK: - Search Bar Delegate
extension SearchSongViewController: UISearchBarDelegate{
    
    internal func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Searching: \(searchText)")
        if searchText.count > 0 {
            renderMode = .hints
            presenter.startGetHints(search: searchText)
        } else {
            self.hints = []
            self.musicItems = []
            renderMode = .clear
            tableView.reloadData()
        }
    }
    
    internal func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        print("New scope index is now \(selectedScope)")
        searchBarScope = SearchBarScope(rawValue: selectedScope) ?? .Songs
        if let hintText = searchController.searchBar.text,  hintText != "" {
            presenter.startGetData(search: hintText)
        }
    }
    
    internal func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.hints = []
        self.musicItems = []
        renderMode = .clear
        tableView.reloadData()
    }
    
    internal func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchController.searchBar.resignFirstResponder()
        if let hintText = searchController.searchBar.text, hintText != "" {
            self.showSpinner()
            presenter.startGetData(search: hintText)
        }
    }
}

// MARK: - Search Bar Delegate
extension SearchSongViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch renderMode {
            case .clear:
                return 0
            case .hints:
                return hints.count
            case .data:
                return musicItems.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch renderMode {
            case .clear:
                return UITableViewCell()
            case .hints:
                let cell = tableView.dequeueReusableCell(withIdentifier: "HintCell", for: indexPath) as! HintsTableViewCell
                let hint = self.hints[indexPath.row]
                cell.configure(hintText: hint)
                return cell
            case .data:
                let cell = tableView.dequeueReusableCell(withIdentifier: "SongCell", for: indexPath) as! SongTableViewCell
                let musicItem = self.musicItems[indexPath.row]
                cell.configure(musicItem: musicItem)
                return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        searchController.searchBar.resignFirstResponder()
        switch renderMode {
            case .clear:
                break
            case .hints:
                self.showSpinner()
                let hintText = self.hints[indexPath.row]
                print("Select hint: \(hintText)")
                searchController.searchBar.text = hintText
                presenter.startGetData(search: hintText)
                break
            case .data:
                switch self.searchBarScope {
                    case .Albums:
                        break
                    case .Artists:
                        let song = self.musicItems[indexPath.row]
                        presenter.startShowArtist(with: song.id)
                        break
                    case .Songs:
                        let musicItem = self.musicItems[indexPath.row]
                        presenter.startShowPlayer(musicItem: musicItem)
                        break
                }
                tableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    func tableView(_ tableView: UITableView, willPerformPreviewActionForMenuWith configuration: UIContextMenuConfiguration, animator: UIContextMenuInteractionCommitAnimating) {
        animator.addCompletion {
            guard let sellection = self.contextMenuSelection else { return }
            switch self.searchBarScope {
                case .Artists:
//                    let viewController = ArtistModule().build(with: song.id)
                    self.presenter.startShowArtist(with: sellection.id)
                case .Albums:
                    return
//                     let viewController = ContexMenuViewController(image: cell.coverImage.image!)
//                     self.show(ContexMenuViewController(image: UIImage()), sender: self)
                case .Songs:
                     let viewController = PlayerModule().build(song: sellection as! Song)
                     self.show(viewController, sender: self)
            }
        }
    }

    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        switch renderMode {
            case .clear:
                return nil
            case .hints:
                return nil
            case .data:
                let cell = tableView.cellForRow(at: indexPath) as! SongTableViewCell
                let musicItem = self.musicItems[indexPath.row]
                contextMenuSelection = musicItem
                return UIContextMenuConfiguration(identifier: nil, previewProvider: { () -> UIViewController? in
                    switch self.searchBarScope {
                        case .Artists:
                            return ArtistModule().build(with: musicItem.id)
                        case .Albums:
                            return ContexMenuViewController(image: cell.coverImage.image!)
                        case .Songs:
//                            return PlayerModule().build(song: song)
                            return ContexMenuViewController(image: cell.coverImage.image!)
                    }
                }) {  suggestedActions in
        
                    // Create an action for sharing
                    let share = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up.on.square")) { action in
                       // Show system share sheet
                    }

                    let isFavorite: Bool = self.presenter.startGetFavoriteState(musicItem: musicItem)
                    let iconName: String = isFavorite ? "suit.heart.fill" : "suit.heart"

                    // Create an action for renaming
                    let favorite = UIAction(title: "Favorite", image: UIImage(systemName: iconName)) { action in
                       // Perform renaming
                        self.presenter.startFavoriteToggle(musicItem: musicItem, type: self.searchBarScope)
                    }

                    // Create and return a UIMenu with all of the actions as children
                    return UIMenu(title: "", children: [share, favorite])
                }
        }
    }
    
    
}

// MARK: - Presenter
extension SearchSongViewController: SearchSongModuleViewPresenterInterface {

}
