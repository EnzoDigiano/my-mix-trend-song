//
//  ContexMenuViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 11/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class ContexMenuViewController: UIViewController {

    @IBOutlet weak var coverImage: UIImageView!
    internal var image : UIImage? = nil
    
//    override func loadView() {
//        view = coverImage
//    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    init(image: UIImage){
        super.init(nibName: nil, bundle: nil)
        
        self.image = image
        preferredContentSize = image.size
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard let coverImage = self.image else { return }
        self.coverImage.image = coverImage
        
        let topShadow = EdgeShadowLayer(forView: self.coverImage, edge: .Top)
        self.coverImage.layer.addSublayer(topShadow)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
