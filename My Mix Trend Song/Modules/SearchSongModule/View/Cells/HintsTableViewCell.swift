//
//  HintsTableViewCell.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 06/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class HintsTableViewCell: UITableViewCell {

    @IBOutlet weak var hintLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(hintText:String){
        self.hintLabel.text = hintText
        self.setDisclosure(toColour: .systemRed)
    }
}
