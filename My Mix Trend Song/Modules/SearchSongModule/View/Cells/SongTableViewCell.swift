//
//  SongTableViewCell.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 06/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit
import Kingfisher

class SongTableViewCell: UITableViewCell {

    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var aditionalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(musicItem: MusicItem){
        self.setDisclosure(toColour: .systemRed)
        self.titleLabel.text = musicItem.name
        self.descriptionLabel.text = musicItem.descriptions
        self.aditionalLabel.text = musicItem.genre
        let url = musicItem.cover
        let urlImage = url.replacingOccurrences(of: "{w}x{h}", with: "800x800")
        self.coverImage.image = UIImage(systemName: "music.note.list")
        if let coverURL = URL(string: urlImage) {
            coverImage.kf.setImage(with: .network(coverURL), options: [.transition(.fade(0.2))]) { result in
                switch result {
                    case .success( _):
                        print("Sucessfully get image")
                    case .failure( _):
                        print("Fail to get image")
                }
            }
        } else {
           print("Fail to make cover URL")
        }
    }
    
}
