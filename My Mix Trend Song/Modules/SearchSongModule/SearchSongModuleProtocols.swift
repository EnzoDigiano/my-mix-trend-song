//
//  SearchSongModuleProtocols.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 05/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import UIKit

// MARK: - router
protocol SearchSongModuleRouterPresenterInterface: RouterPresenterInterface {
    func showPlayer(song: Song)
    func showArtist(with id:String)
}

// MARK: - interactor
protocol SearchSongModuleInteractorPresenterInterface: InteractorPresenterInterface {
    func getHints(search: String)
    func getData(search: String)
    func makeSetFavorite(musicItem: MusicItem, type: SearchBarScope)
    func makeGetFavoriteState(musicItem: MusicItem) -> Bool
}

// MARK: - presenter
protocol SearchSongModulePresenterRouterInterface: PresenterRouterInterface {

}

protocol SearchSongModulePresenterInteractorInterface: PresenterInteractorInterface {
    func didGetHints(result: Result<[String], Error>)
    func didGetData(result: Result<SearchSongsResponseResults, Error>)
    func didSetFavorite(state: Bool)
}

protocol SearchSongModulePresenterViewInterface: PresenterViewInterface {
    func startGetHints(search: String)
    func startGetData(search: String)
    func startShowPlayer(musicItem: MusicItem)
    func startShowArtist(with id:String)
    func startFavoriteToggle(musicItem: MusicItem, type: SearchBarScope)
    func startGetFavoriteState(musicItem: MusicItem) -> Bool
}

// MARK: - view
protocol SearchSongModuleViewPresenterInterface: ViewPresenterInterface {
    var hints: [String] { get set }
    var musicItems: [MusicItem] { get set }
    var renderMode: RenderSearchResult { get set }
    var searchBarScope : SearchBarScope { get set }
}
