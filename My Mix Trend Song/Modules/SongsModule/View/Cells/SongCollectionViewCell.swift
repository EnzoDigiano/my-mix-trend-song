//
//  SongCollectionViewCell.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 02/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit
import Kingfisher
class SongCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var cover: UIImageView!
    var song: Song? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configure(song: Song){
        self.song = song
        self.getCoverImage(song.cover)
        self.shadowDecorate()
    }
    
    fileprivate func getCoverImage(_ path: String){
        let url = path
        let urlImage = url.replacingOccurrences(of: "{w}x{h}", with: "800x800")
        if let coverURL = URL(string: urlImage) {
            cover.kf.setImage(with: .network(coverURL), options: [.transition(.fade(0.2))]) { result in
                switch result {
                    case .success( _):
                        print("Sucessfully get image")
                    case .failure( _):
                        print("Fail to get image")
                }
            }
        } else {
           print("Fail to make cover URL")
        }
    }
}
