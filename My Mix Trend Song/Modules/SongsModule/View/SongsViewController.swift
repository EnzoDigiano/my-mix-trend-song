//
//  SongsViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class SongsViewController: UIViewController, ViewInterface {

    var presenter: SongsModulePresenterViewInterface!
    var songs:[Song]? = nil { didSet { self.reloadData() } }
    var dataSource: UICollectionViewDiffableDataSource<Section,Song>! = nil
    
    @IBOutlet weak var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter.startGetSongs()
        configureCollectionView()
        configureDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationItem.largeTitleDisplayMode = .automatic
        navigationController?.navigationBar.isTranslucent = true
//        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.clear]
    }
}

// MARK: - Presenter
extension SongsViewController: SongsModuleViewPresenterInterface {

}

// MARK: - Collection View Delegate
extension SongsViewController: UICollectionViewDelegate {

    fileprivate func configureCollectionView() {
        collectionView.backgroundColor = .clear
        collectionView.register(UINib(nibName: "SongCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SongCell")
        collectionView.register(UINib(nibName: "SongHeaderCollectionReusableView", bundle: nil),
        forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
        withReuseIdentifier: "SectionHeader")
        collectionView.collectionViewLayout = createCompositionalLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Select item at index \(indexPath)")
        if let cell = collectionView.cellForItem(at: indexPath) as? SongCollectionViewCell,
           let song = cell.song {
            
            print("Cell Selected")
            presenter.startShowPlayer(song: song)
        }
    }
    
}

// MARK: - Flow Layout
extension SongsViewController {
    
    fileprivate func createCompositionalLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { sectionNumber, env -> NSCollectionLayoutSection? in
            switch Section(rawValue: sectionNumber) {
                case .main:
                    return self.gridLayoutSection()
                case .gallery:
                    return self.orthogonalLayout()
                default:
                    return nil
            }
        }
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = 0
        layout.configuration = config
        return layout
    }

    fileprivate func gridLayoutSection() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 10)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.93),
                                               heightDimension: .estimated(300))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitem: item,
                                                       count: 2)
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary
        section.contentInsets = NSDirectionalEdgeInsets(top: 18, leading: 20, bottom: 0, trailing: 20)
        let headerSize =  NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                 heightDimension: .estimated(40))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize,
                                                                 elementKind: UICollectionView.elementKindSectionHeader,
                                                                 alignment: .top)
        header.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
        section.boundarySupplementaryItems = [header]
        return section
    }
    
    fileprivate func orthogonalLayout() -> NSCollectionLayoutSection {
        let leadingItem = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.665), heightDimension: .fractionalHeight(1.0)))
        leadingItem.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 8)

        let trailingItem = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.5)))
        trailingItem.contentInsets = NSDirectionalEdgeInsets(top: 8, leading: 8, bottom: 8, trailing: 0)

        let trailingGroup = NSCollectionLayoutGroup.vertical(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.335), heightDimension: .fractionalHeight(1.0)), subitem: trailingItem, count: 2)

        let nestedGroup = NSCollectionLayoutGroup.horizontal(layoutSize: NSCollectionLayoutSize(widthDimension: .fractionalWidth(1), heightDimension: .fractionalWidth(0.665)), subitems: [leadingItem, trailingGroup])

        let section = NSCollectionLayoutSection(group: nestedGroup)
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 20, bottom: 0, trailing: 20)
        let headerSize =  NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                 heightDimension: .estimated(40))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize,
                                                                 elementKind: UICollectionView.elementKindSectionHeader,
                                                                 alignment: .top)
        header.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 0, bottom: 0, trailing: 0)
        section.boundarySupplementaryItems = [header]
        return section
    }
}

// MARK: - Diffable Data Source
extension SongsViewController {
    
    fileprivate func configureDataSource() {
        self.dataSource = UICollectionViewDiffableDataSource<Section, Song>(collectionView: self.collectionView){ collectionView, indexPath, item in
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SongCell", for: indexPath) as? SongCollectionViewCell else { fatalError("Couldn't dequeue reusable cell") }
            cell.configure(song: item)
            return cell
        }
        self.dataSource.supplementaryViewProvider = { [weak self] collectionView, kind, indexPath in
            guard let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                                      withReuseIdentifier: "SectionHeader",
                                                                                      for: indexPath) as? SongHeaderCollectionReusableView else { return nil}
            switch Section.init(rawValue:indexPath.section){
                case .main:
                    sectionHeader.configure(section: "Top 10")
                case .gallery:
                    sectionHeader.configure(section: "More Hits")

                case .none:
                    return nil
            }
            
            return sectionHeader
        }
    }
    
    fileprivate func reloadData() {
        if let items = self.songs {
            var snapshot = NSDiffableDataSourceSnapshot<Section, Song>()
            
            var mainSongs = [Song]()
            for item in items[0..<10] {
                mainSongs.append(item)
            }
            snapshot.appendSections([.main])
            snapshot.appendItems(mainSongs)
                
            var gallerySongs = [Song]()
            for item in items[10..<49] {
                gallerySongs.append(item)
            }
            snapshot.appendSections([.gallery])
            snapshot.appendItems(gallerySongs)
                
            dataSource.apply(snapshot, animatingDifferences: true)
        }
    }
}
