//
//  SongsModulePresenter.swift
//  SongsModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation

final class SongsModulePresenter: PresenterInterface {

    var router: SongsModuleRouterPresenterInterface!
    var interactor: SongsModuleInteractorPresenterInterface!
    weak var view: SongsModuleViewPresenterInterface!
}

//MARK: - Router
extension SongsModulePresenter: SongsModulePresenterRouterInterface {

}

//MARK: - Interactor
extension SongsModulePresenter: SongsModulePresenterInteractorInterface {
    
    func didGetSongs(result: Result<[Song], Error>) {
        switch result {
            case .success(let songs):
                view.songs = songs
            case .failure(let error):
                print("Couldn't get songs: \(error)")
                break
        }
    }
}

//MARK: - View
extension SongsModulePresenter: SongsModulePresenterViewInterface {
    
    func startGetSongs() {
        interactor.getSongs()
    }
    
    func startShowPlayer(song: Song) {
        router.showPlayer(song: song)
    }
}
