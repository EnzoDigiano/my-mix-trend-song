//
//  SongModuleProtocols.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 01/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

// MARK: - router
protocol SongsModuleRouterPresenterInterface: RouterPresenterInterface {
    func showPlayer(song: Song)
}

// MARK: - interactor
protocol SongsModuleInteractorPresenterInterface: InteractorPresenterInterface {

    func getSongs()
}

// MARK: - presenter
protocol SongsModulePresenterRouterInterface: PresenterRouterInterface {

}

protocol SongsModulePresenterInteractorInterface: PresenterInteractorInterface {
    func didGetSongs(result: Result<[Song], Error>)
}

protocol SongsModulePresenterViewInterface: PresenterViewInterface {
    func startGetSongs()
    func startShowPlayer(song: Song)
}

// MARK: - view
protocol SongsModuleViewPresenterInterface: ViewPresenterInterface {
    var songs:[Song]? { get set }
}
