//
//  Sections.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 02/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

enum Section: Int{
    case main = 0
    case gallery = 1
}
