//
//  SongsModuleInteractor.swift
//  SongsModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation
import Realm
import PromiseKit

final class SongsModuleInteractor: InteractorInterface {

    weak var presenter: SongsModulePresenterInteractorInterface!
}

//MARK: - Presenter
extension SongsModuleInteractor: SongsModuleInteractorPresenterInterface {

    func getSongs() {
        let service = AppleMusicService()
        firstly {
            service.fetchHotTracks()
        }.done { response in
            var songs: [Song] = []
            guard let data = response.data?.first?.relationships?.contents?.data else {
                print("There be an error parsing data")
                self.presenter?.didGetSongs(result:.failure(ResponseError.getData))
                return
            }
            for item in data { songs.append(item.toSong()) }
            self.presenter?.didGetSongs(result:.success(songs))
        }.catch { error in
            print("There be an error getting songs: \(error.localizedDescription)")
            self.presenter?.didGetSongs(result:.failure(error))
        }
    }
}
