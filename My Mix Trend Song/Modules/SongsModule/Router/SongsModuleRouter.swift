//
//  SongsModuleRouter.swift
//  SongsModule
//
//  Created by Enzo Digiano on 1/8/20.
//

import Foundation
import UIKit

final class SongsModuleRouter: RouterInterface {

    weak var presenter: SongsModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

//MARK: - Presenter
extension SongsModuleRouter: SongsModuleRouterPresenterInterface {
    
    func showPlayer(song: Song) {
        let playerViewController = PlayerModule().build(song: song)
        self.viewController?.navigationController?.pushViewController(playerViewController, animated: true)
    }
}
