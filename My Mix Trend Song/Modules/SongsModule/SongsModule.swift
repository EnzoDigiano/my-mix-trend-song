//
//  SongsModuleModule.swift
//  SongsModule
//
//  Created by Enzo Digiano on 1/8/20.
//
import Foundation
import UIKit


// MARK: - module builder

final class SongsModule: ModuleInterface {

    typealias View = SongsViewController
    typealias Presenter = SongsModulePresenter
    typealias Router = SongsModuleRouter
    typealias Interactor = SongsModuleInteractor

    func build() -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        view.title = "My Mix Trend Song" 
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}

#if DEBUG
import SwiftUI

@available(iOS 13, *)
struct SongsModulePreview: PreviewProvider {
    
    static var devices = ["iPhone 11 Pro Max"]
    
    static var previews: some View {
        ForEach(devices, id: \.self) { deviceName in
            SongsModule().build().toPreview()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
#endif
