//
//  FavoriteModuleProtocols.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import UIKit

// MARK: - router
protocol FavoriteModuleRouterPresenterInterface: RouterPresenterInterface {

}

// MARK: - interactor
protocol FavoriteModuleInteractorPresenterInterface: InteractorPresenterInterface {
    func makeGetFavorites()
}

// MARK: - presenter
protocol FavoriteModulePresenterRouterInterface: PresenterRouterInterface {
    
}

protocol FavoriteModulePresenterInteractorInterface: PresenterInteractorInterface {
    func didGetFavorites(favorites: [Favorite])
}

protocol FavoriteModulePresenterViewInterface: PresenterViewInterface {
    func startGetFavorites()
}

// MARK: - view
protocol FavoriteModuleViewPresenterInterface: ViewPresenterInterface {
    var favorites: [Favorite] { get set}
}
