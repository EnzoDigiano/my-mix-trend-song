//
//  FavoriteModuleRouter.swift
//  FavoriteModule
//
//  Created by Enzo Nicolas Digiano on 10/8/20.
//

import Foundation
import UIKit

final class FavoriteModuleRouter: RouterInterface {

    weak var presenter: FavoriteModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension FavoriteModuleRouter: FavoriteModuleRouterPresenterInterface {

}
