//
//  FavoriteModuleSections.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation

enum FavoriteModuleSection: Int{
    case Artists = 0
    case Albums = 1
    case Songs = 2
}
