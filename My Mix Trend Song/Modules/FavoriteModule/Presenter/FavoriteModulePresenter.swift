//
//  FavoriteModulePresenter.swift
//  FavoriteModule
//
//  Created by Enzo Nicolas Digiano on 10/8/20.
//

import Foundation

final class FavoriteModulePresenter: PresenterInterface {

    var router: FavoriteModuleRouterPresenterInterface!
    var interactor: FavoriteModuleInteractorPresenterInterface!
    weak var view: FavoriteModuleViewPresenterInterface!
}

// MARK: Router
extension FavoriteModulePresenter: FavoriteModulePresenterRouterInterface {

}

// MARK: Interactor
extension FavoriteModulePresenter: FavoriteModulePresenterInteractorInterface {
    
    func didGetFavorites(favorites: [Favorite]) {
        view.favorites = favorites
    }
}

// MARK: View
extension FavoriteModulePresenter: FavoriteModulePresenterViewInterface {
    
    func startGetFavorites() {
        interactor.makeGetFavorites()
    }
}
