//
//  FavoriteModuleViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class FavoriteModuleViewController: UIViewController, ViewInterface {

    var presenter: FavoriteModulePresenterViewInterface!
    var favorites: [Favorite] = [] { didSet{
            print("Set favorites")
            reloadData()
        }
    }
    var sections: [Int] = []
    var dataSource: UICollectionViewDiffableDataSource<FavoriteModuleSection,Favorite>! = nil

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureCollectionView()
        configureDataSource()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationItem.title = "Favorites"
        presenter.startGetFavorites()
        
    }
}

// MARK: - View
extension FavoriteModuleViewController: FavoriteModuleViewPresenterInterface {

}

// MARK: - Collection View Delegate
extension FavoriteModuleViewController: UICollectionViewDelegate {

    fileprivate func configureCollectionView() {
        collectionView.backgroundColor = .clear
        collectionView.register(UINib(nibName: "FavoriteSongCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FavoriteSongCell")
        collectionView.register(UINib(nibName: "FavoriteArtistCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FavoriteArtistCell")
        collectionView.register(UINib(nibName: "FavoriteAlbunCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FavoriteAlbunCell")
        
        collectionView.register(UINib(nibName: "SectionHeaderCollectionReusableView", bundle: nil),
                                forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                                withReuseIdentifier: "SectionHeader")
        
        collectionView.collectionViewLayout = createCompositionalLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }
}

// MARK: - Flow Layout
extension FavoriteModuleViewController {
    
    fileprivate func createCompositionalLayout() -> UICollectionViewLayout {
        let layout =  UICollectionViewCompositionalLayout { sectionIndex, layoutEnvironment -> NSCollectionLayoutSection? in
            let section = self.sections[sectionIndex]
            switch FavoriteModuleSection(rawValue: section) {
                case .Artists:
                    return self.artistLayout()
                case .Albums:
                    return self.albunsLayout()
                case .Songs:
                    return self.songsLayout()
                default:
                    return nil
            }
        }
        let config = UICollectionViewCompositionalLayoutConfiguration()
        config.interSectionSpacing = 0
        layout.configuration = config
        return layout
    }

    fileprivate func artistLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                              heightDimension: .fractionalWidth(0.45))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 5, bottom: 0, trailing: 5)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.93),
                                               heightDimension: .fractionalWidth(0.45))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitem: item,
                                                       count: 1)
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .groupPagingCentered
        section.contentInsets = NSDirectionalEdgeInsets(top: 15, leading: 0, bottom: 0, trailing: 0)
        let headerSize =  NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                 heightDimension: .estimated(40))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize,
                                                                 elementKind: UICollectionView.elementKindSectionHeader,
                                                                 alignment: .top)
        header.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)
        section.boundarySupplementaryItems = [header]
        return section
    }
    
    fileprivate func albunsLayout() -> NSCollectionLayoutSection {
       let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.93),
                                               heightDimension: .estimated(200))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize,
                                                       subitem: item,
                                                       count: 2)
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .groupPagingCentered
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0)
        let headerSize =  NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                 heightDimension: .estimated(40))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize,
                                                                 elementKind: UICollectionView.elementKindSectionHeader,
                                                                 alignment: .top)
        header.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)
        section.boundarySupplementaryItems = [header]
        return section
    }
    
    fileprivate func songsLayout() -> NSCollectionLayoutSection {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                              heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 8, trailing: 20)
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0),
                                               heightDimension: .estimated(60))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                       subitems: [item])
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 0, bottom: 0, trailing: 0)
        let headerSize =  NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                 heightDimension: .estimated(40))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize,
                                                                 elementKind: UICollectionView.elementKindSectionHeader,
                                                                 alignment: .top)
        header.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 20, bottom: 0, trailing: 20)
        section.boundarySupplementaryItems = [header]
        return section
    }
}

// MARK: - Diffable Data Source
extension FavoriteModuleViewController {
    
    fileprivate func configureDataSource() {
        self.dataSource = UICollectionViewDiffableDataSource<FavoriteModuleSection, Favorite>(collectionView: self.collectionView) { collectionView, indexPath, favorite in
            switch FavoriteModuleSection.init(rawValue: self.sections[indexPath.section]){
                case .Artists:
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteArtistCell", for: indexPath) as? FavoriteArtistCollectionViewCell else { fatalError("Couldn't dequeue reusable cell") }
                    cell.configure(with: favorite)
                    return cell
                case .Albums:
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteAlbunCell", for: indexPath) as? FavoriteAlbunCollectionViewCell else { fatalError("Couldn't dequeue reusable cell") }
                    cell.configure(with: favorite)
                    return cell
                case.Songs:
                    guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoriteSongCell", for: indexPath) as? FavoriteSongCollectionViewCell else { fatalError("Couldn't dequeue reusable cell") }
                    cell.configure(with: favorite)
                    return cell
                case .none:
                    return UICollectionViewCell()
            }
        }
        self.dataSource.supplementaryViewProvider = { [weak self] collectionView, kind, indexPath in
            guard let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                                                      withReuseIdentifier: "SectionHeader",
                                                                                      for: indexPath) as? SectionHeaderCollectionReusableView else { return nil}
            switch FavoriteModuleSection.init(rawValue: self!.sections[indexPath.section]){
                case .Albums:
                    sectionHeader.configure(section: "Albuns")
                case .Artists:
                    sectionHeader.configure(section: "Artists")
                case .Songs:
                    sectionHeader.configure(section: "Songs")
                case .none:
                    return nil
            }
            
            return sectionHeader
        }
    }
    
    fileprivate func reloadData() {
        sections = []
        var snapshot = NSDiffableDataSourceSnapshot<FavoriteModuleSection, Favorite>()
        // Artist
        let favoriteArtist = self.favorites.filter { item -> Bool in
            item.type == FavoriteModuleSection.Artists.rawValue
        }
        if favoriteArtist.count > 0 {
            snapshot.appendSections([.Artists])
            snapshot.appendItems(favoriteArtist)
            sections.append(FavoriteModuleSection.Artists.rawValue)
        }
        // Albuns
        let favoriteAlbuns = self.favorites.filter { item -> Bool in
            item.type == FavoriteModuleSection.Albums.rawValue
        }
        if favoriteAlbuns.count > 0 {
            snapshot.appendSections([.Albums])
            snapshot.appendItems(favoriteAlbuns)
            sections.append(FavoriteModuleSection.Albums.rawValue)
        }
        // Songs
        let favoriteSongs = self.favorites.filter { item -> Bool in
            item.type == FavoriteModuleSection.Songs.rawValue
        }
        if favoriteSongs.count > 0 {
            snapshot.appendSections([.Songs])
            snapshot.appendItems(favoriteSongs)
            sections.append(FavoriteModuleSection.Songs.rawValue)
        }
        dataSource.apply(snapshot, animatingDifferences: true)
    }
}
