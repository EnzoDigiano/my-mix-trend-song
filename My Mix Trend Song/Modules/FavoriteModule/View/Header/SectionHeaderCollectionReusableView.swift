//
//  SectionHeaderCollectionReusableView.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class SectionHeaderCollectionReusableView: UICollectionReusableView {

    @IBOutlet weak var titleSectionLabel: UILabel!
    @IBOutlet weak var showMoreSectionBucton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func showMoreButtonDidTap(_ sender: Any) {
        
    }
    
    func configure(section:String){
        titleSectionLabel.text = section
    }
}
