//
//  FavoriteArtistCollectionViewCell.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 10/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class FavoriteArtistCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var songNameLabel: UILabel!
    @IBOutlet weak var songDescriptionLabel: UILabel!
    @IBOutlet weak var songCoverImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configure(with item: Favorite){
        self.songNameLabel.text = item.tile
        self.songDescriptionLabel.text = item.descriptions
        self.getCoverImage(item.cover)
    }
    
    fileprivate func getCoverImage(_ path: String){
        let url = path
        let urlImage = url.replacingOccurrences(of: "{w}x{h}", with: "800x800")
        if let coverURL = URL(string: urlImage) {
            songCoverImage.kf.setImage(with: .network(coverURL), options: [.transition(.fade(0.2))]) { result in
                switch result {
                    case .success( _):
                        print("Sucessfully get image")
                    case .failure( _):
                        print("Fail to get image")
                }
            }
        } else {
           print("Fail to make cover URL")
        }
    }

}
