//
//  FavoriteModuleModule.swift
//  FavoriteModule
//
//  Created by Enzo Nicolas Digiano on 10/8/20.
//
import Foundation
import UIKit

// MARK: - module builder
final class FavoriteModule: ModuleInterface {

    typealias View = FavoriteModuleViewController
    typealias Presenter = FavoriteModulePresenter
    typealias Router = FavoriteModuleRouter
    typealias Interactor = FavoriteModuleInteractor

    func build() -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}
