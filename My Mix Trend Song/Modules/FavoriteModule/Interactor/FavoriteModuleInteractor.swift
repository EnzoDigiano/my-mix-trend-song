//
//  FavoriteModuleInteractor.swift
//  FavoriteModule
//
//  Created by Enzo Nicolas Digiano on 10/8/20.
//

import Foundation

final class FavoriteModuleInteractor: InteractorInterface {

    weak var presenter: FavoriteModulePresenterInteractorInterface!
}

// MARK: - Presenter
extension FavoriteModuleInteractor: FavoriteModuleInteractorPresenterInterface {
    
    func makeGetFavorites() {
        let favoriteService = FavoriteService()
        let favorites = favoriteService.getAll()
        presenter.didGetFavorites(favorites: favorites)
    }
}
