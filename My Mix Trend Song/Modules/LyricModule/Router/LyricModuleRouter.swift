//
//  LyricModuleRouter.swift
//  LyricModule
//
//  Created by Enzo Digiano on 4/8/20.
//

import Foundation
import UIKit

final class LyricModuleRouter: RouterInterface {

    weak var presenter: LyricModulePresenterRouterInterface!
    weak var viewController: UIViewController?
}

// MARK: - Presenter
extension LyricModuleRouter: LyricModuleRouterPresenterInterface {

    func dismiss(){
        self.viewController?.dismiss(animated: true, completion: nil)
    }
    
    func share(lyric: String){
        let items: [Any] = [lyric]
        let activityController = UIActivityViewController(activityItems: items, applicationActivities: nil)
        self.viewController?.present(activityController, animated: true)
    }
}
