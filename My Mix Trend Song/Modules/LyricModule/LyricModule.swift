//
//  LyricModuleModule.swift
//  LyricModule
//
//  Created by Enzo Digiano on 4/8/20.
//
import Foundation
import UIKit

// MARK: - module builder
final class LyricModule: ModuleInterface {

    typealias View = LyricViewController
    typealias Presenter = LyricModulePresenter
    typealias Router = LyricModuleRouter
    typealias Interactor = LyricModuleInteractor

    func build(lyric: String) -> UIViewController {
        let view = View()
        let interactor = Interactor()
        let presenter = Presenter()
        let router = Router()

        view.lyric = lyric
        view.title = "Lyric"
        
        self.assemble(view: view, presenter: presenter, router: router, interactor: interactor)
        router.viewController = view
        return view
    }
}

#if DEBUG
import SwiftUI

@available(iOS 13, *)
struct LyricModulePreview: PreviewProvider {
    
    static var devices = ["iPhone 11 Pro Max"]
    
    static var previews: some View {
        ForEach(devices, id: \.self) { deviceName in
            LyricModule().build(lyric: "Lyric mock").toPreview()
                .previewDevice(PreviewDevice(rawValue: deviceName))
                .previewDisplayName(deviceName)
                .edgesIgnoringSafeArea(.all)
        }
    }
}
#endif
