//
//  LyricModulePresenter.swift
//  LyricModule
//
//  Created by Enzo Digiano on 4/8/20.
//

import Foundation

final class LyricModulePresenter: PresenterInterface {

    var router: LyricModuleRouterPresenterInterface!
    var interactor: LyricModuleInteractorPresenterInterface!
    weak var view: LyricModuleViewPresenterInterface!
}

// MARK: - Router
extension LyricModulePresenter: LyricModulePresenterRouterInterface {

}

// MARK: - Interactor
extension LyricModulePresenter: LyricModulePresenterInteractorInterface {

}

// MARK: - View
extension LyricModulePresenter: LyricModulePresenterViewInterface {

    func startDismiss(){
        router.dismiss()
    }
    
    func startShare(lyric: String){
        router.share(lyric: lyric)
    }
}
