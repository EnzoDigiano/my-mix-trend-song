//
//  LyricViewController.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 04/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import UIKit

class LyricViewController: UIViewController, ViewInterface {

    var presenter: LyricModulePresenterViewInterface!
    var lyric: String? = nil

    @IBOutlet weak var lyricTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        guard let lyric = self.lyric else { return }
        self.lyricTextView.text = lyric
        addNavigationBarRightButton()
    }
    
    internal func addNavigationBarRightButton(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(close))
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "square.and.arrow.up.on.square"), style: .plain, target: self, action: #selector(share))
        navigationController?.navigationBar.tintColor = .systemRed
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.systemRed]
    }
    
    @objc internal func close(){
        presenter.startDismiss()
    }
    
    @objc internal func share(){
        guard let lyric = self.lyric else { return }
        presenter.startShare(lyric: lyric)
    }
}

// MARK: - Presenter
extension LyricViewController: LyricModuleViewPresenterInterface {

}
