//
//  LyricModuleInteractor.swift
//  LyricModule
//
//  Created by Enzo Digiano on 4/8/20.
//

import Foundation

final class LyricModuleInteractor: InteractorInterface {

    weak var presenter: LyricModulePresenterInteractorInterface!
}

// MARK: - Presenter
extension LyricModuleInteractor: LyricModuleInteractorPresenterInterface {

}
