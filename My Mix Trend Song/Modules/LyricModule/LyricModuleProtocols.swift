//
//  LyricModuleProtocols.swift
//  My Mix Trend Song
//
//  Created by Enzo Digiano on 04/08/2020.
//  Copyright © 2020 EnzoDigiano. All rights reserved.
//

import Foundation
import UIKit

// MARK: - router
protocol LyricModuleRouterPresenterInterface: RouterPresenterInterface {
    func dismiss()
    func share(lyric: String)
}

// MARK: - interactor
protocol LyricModuleInteractorPresenterInterface: InteractorPresenterInterface {

}

// MARK: - presenter
protocol LyricModulePresenterRouterInterface: PresenterRouterInterface {

}

protocol LyricModulePresenterInteractorInterface: PresenterInteractorInterface {

}

protocol LyricModulePresenterViewInterface: PresenterViewInterface {
    func startDismiss()
    func startShare(lyric: String)
}

// MARK: - view
protocol LyricModuleViewPresenterInterface: ViewPresenterInterface {
    var lyric: String? { get set}
}
